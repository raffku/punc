/*
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2023 Raffin Kuno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

include "module";
include "location";
include "token";
include "result";
include "../tools/baseutils";
include "coll/queue";
include "std/string";
include "coll/strings";

include "std/logger"; Logger log.reg();

public type Statement {
    Location loc;
    Queue[Token] tokens;
on_move:
    self.loc = other.loc;
    self.tokens.clr().swap(other.tokens);
}

public augment Statement {
    inline Location get_location(const Self self) { return self.loc; }
    inline bool empty(const Self self) { return self.tokens.empty(); }
    inline uint size(const Self self) { return self.tokens.size(); }

    Self* from_module(Self* self != null, Module module) {
        self.loc.within(module);
        return self;
    }

    Self* from_location(Self* self != null, Location location) {
        self.loc = location;
        return self;
    }

    Self* extend_location_to(Self* self != null, Location end_location) {
        self.loc.to_location(end_location);
        return self;
    }

    const Token* get_first_token(const Self* self != null) {
        return self.tokens.head();
    }

    Token* mod_first_token(Self* self != null) {
        return self.tokens.mod_head();
    }

    const Token* get_second_token(const Self* self != null) {
        return ((self.tokens.size() > 1) ? self.tokens.at(1) : null);
    }

    Token* mod_second_token(Self* self != null) {
        return ((self.tokens.size() > 1) ? self.tokens.mod_at(1) : null);
    }

    const Token* get_third_token(const Self* self != null) {
        return ((self.tokens.size() > 2) ? self.tokens.at(2) : null);
    }

    const Token* get_last_token(const Self* self != null) {
        return self.tokens.tail();
    }

    Token* mod_last_token(Self* self != null) {
        return self.tokens.mod_tail();
    }

    inline const Queue[Token]* get_tokens(const Self* self != null) {
        return &self.tokens;
    }

    Token* new_token(Self* self != null, Location location, String text, TokenKind kind) {
        auto token = self.tokens.new() or exit(EXIT_OOM);
        if (self.tokens.size() == 1) {
            self.loc = location;
        } else {
            self.loc.to_location(location);
        }
        return token.from_location_text_kind(location,text,kind);
    }

    Token* put_token(Self* self != null, Token token) {
        auto result = self.tokens.new() or exit(EXIT_OOM);
        if (self.tokens.size() == 1) {
            self.loc = token.get_location();
        } else {
            self.loc.to_location(token.get_location());
        }
        *result = token;
        return result;
    }

    Token* pop_token(Self* self != null) {
        Token* token = self.tokens.pop();
        if (auto first_token = self.tokens.head()) {
            self.loc.from_location(first_token.get_location());
        } else {
            self.loc.from(self.loc.get_to_lineno(),self.loc.get_to_column()).to(0,0);
        }
        return token;
    }

    Self* skim_token(Self* self != null, Self* other != null) {
        self.tokens.skim(other.tokens);
        if (self.tokens.size() == 1) {
            self.loc = self.tokens.head().get_location();
        } else {
            self.loc.to_location(self.tokens.tail().get_location());
        }
        if (const Token* first_token = other.tokens.head()) {
            other.loc.from_location(first_token.get_location());
        } else {
            other.loc.from(self.loc.get_to_lineno(),self.loc.get_to_column()).to(0,0);
        }
        return self;
    }

    Self* join_tokens(Self* self != null, Self* other != null) {
        until (other.empty()) {
            self.skim_token(other);
        }
        return self;
    }

    Self* swap_tokens(Self* self != null, Self* other != null) {
        self.tokens.swap(other.tokens);
        Location loc = other.loc;
        other.loc = self.loc;
        self.loc = loc;
        return self;
    }
}

// parse helpers
public augment Statement {
    Self split_from(Self* self != null, TokenKind kind, uint skip) {
        Statement rest;
        while (const Token* token = self.tokens.head()) {
            if (token.get_kind() == kind && skip-- == 0) {
                break;
            }
            rest.skim_token(self);
        }
        // swap self and rest and return
        self.swap_tokens(rest);
        return rest;
    }

    Self split_from_op(Self* self != null, const char* op, uint skip) {
        Statement rest;
        while (const Token* token = self.tokens.head()) {
            if (token.get_kind() == OP && token.get_text().eqs(op) && skip-- == 0) {
                break;
            }
            rest.skim_token(self);
        }
        // swap self and rest and return
        self.swap_tokens(rest);
        return rest;
    }

    Self split_from_block(Self* self != null, const char* typ, uint skip) {
        Statement rest;
        while (const Token* token = self.tokens.head()) {
            if (token.get_kind() == BLOCK && token.get_text().eqs(typ) && skip-- == 0) {
                break;
            }
            rest.skim_token(self);
        }
        // swap self and rest and return
        self.swap_tokens(rest);
        return rest;
    }

    Self split_from_key(Self* self != null, const char* typ, uint skip) {
        Statement rest;
        while (const Token* token = self.tokens.head()) {
            if (token.get_kind() == KEY && token.get_text().eqs(typ) && skip-- == 0) {
                break;
            }
            rest.skim_token(self);
        }
        // swap self and rest and return
        self.swap_tokens(rest);
        return rest;
    }

    void split(Self* self != null, TokenKind kind, Queue[Statement]* statements != null) {
        until (self.empty()) {
            Statement rest = self.split_from(kind,0);
            Statement* res = statements.new() or exit(EXIT_OOM);
            *res = *self;
            rest.pop_token();   // pop the token
            *self = rest;       // repeat with the rest of the statement
        }
    }

    void split_op(Self* self != null, const char* op, Queue[Statement]* statements != null) {
        until (self.empty()) {
            Statement rest = self.split_from_op(op,0);
            Statement* res = statements.new() or exit(EXIT_OOM);
            *res = *self;
            rest.pop_token();   // pop the op
            *self = rest;       // repeat with the rest of the statement
        }
    }

    Self split_first(Self* self != null) {
        Statement head.skim_token(self);
        return head;
    }

    Self split_last(Self* self != null) {
        Statement rest;
        while (self.tokens.head() != self.tokens.tail()) {
            rest.skim_token(self);
        }
        // swap self and rest and return
        self.swap_tokens(rest);
        return rest;
    }

    String get_ident_before_op(Self* self != null, const char* op) {
        String ident;
        Self rest = self.split_from_op(op,0);
        unless (rest.empty()) {
            const Token* token = self.get_last_token();
            if (token.get_kind() == IDENT) {
                ident = token.get_text();
            }
            self.join_tokens(rest);
        }
        return ident;
    }
}

// display
public String Statement::get_display(const Self self) {
    Queue[String] result;
    int space_before = 0;
    for (auto iter = self.get_tokens().iter(); iter.get(); iter.next()) {
        if (iter.get().get_text().eqs(".")) {
            result.new().cpy(iter.get().get_text());
            space_before = 0;
        } else if (iter.get().get_text().eqs(",") || iter.get().get_text().eqs(":base") || iter.get().get_text().eqs(":disp")) {
            result.new().cpy(iter.get().get_text());
            space_before = 1;
        } else if (iter.get().get_kind() == BLOCK) {
            if (space_before > (iter.get().get_text().ends_with("(") ? 1: 0)) {
                result.new().set(" ");
            }
            result.new().cpy(iter.get().get_text());
            const Queue[Statement]* statements = iter.get().get_statements();
            if (statements.size() > 1) {
                result.new().set("...");
            } else if (statements.size() == 1 && iter.get().get_text().ends_with("{")) {
                result.new().set("...");
            } else if (statements.size() == 1) {
                result.new().cpy(statements.head().get_display());
            }
            if (iter.get().get_text().ends_with("(")) {
                result.new().set(")");
            } else if (iter.get().get_text().ends_with("[")) {
                result.new().set("]");
            } else if (iter.get().get_text().ends_with("{")) {
                result.new().set("}");
            }
            space_before = 1;
        } else {
            if (space_before > 0) {
                result.new().set(" ");
            } else {
                space_before = 1;
            }
            result.new().cpy(iter.get().get_text());
            if (iter.get().get_kind() == OP || iter.get().get_kind() == KEY) {
                space_before = 2;
            }
        }
    }
    return String::join(result,"");
}

public test "Statement::get_display" with {
    include "std/string";
    include "std/logger"; Logger log.reg();
} do {
    Statement stmt1.from_string("const int x = 0;");
    assert stmt1.get_display().eqs("const int x = 0");
    //log.info("%s",stmt1 .get_display().str());

    Statement stmt2.from_string("a.x()");
    assert stmt2.get_display().eqs("a.x()");
    //log.info("%s",stmt2.get_display().str());

    Statement stmt3.from_string("(1+0) + (3*5)");
    assert stmt3.get_display().eqs("(1 + 0) + (3 * 5)");
    //log.info("%s",stmt3.get_display().str());

    Statement stmt4.from_string("-5");
    assert stmt4.get_display().eqs("- 5"); // -5 would be nicer but not worth it
    //log.info("%s",stmt4.get_display().str());

    Statement stmt5.from_string("{}");
    assert stmt5.get_display().eqs("{}");
    //log.info("%s",stmt5.get_display().str());

    Statement stmt6.from_string("while (a > 0) {}");
    assert stmt6.get_display().eqs("while (a > 0) {}");
    //log.info("%s",stmt6.get_display().str());

    Statement stmt7.from_string("ratio(1,2)");
    assert stmt7.get_display().eqs("ratio(1, 2)");
    //log.info("%s",stmt7.get_display().str());
}

//////////////////////////////////

#[PUNC_TEST] {
    include "../parser/parser";
    include "module";

    public void Statement::from_string(Statement* self != null, const char* content != null) {
        Module module.set_name(String::of("(test)"));
        assert Queue[Statement] queue.from_string(module,content).no_msg();
        assert queue.size() == 1;
        Statement* stmt = queue.mod_head() and *self = *stmt;
    }
}

