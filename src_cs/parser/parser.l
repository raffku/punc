/*
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

%option noyywrap
%option yylineno
%option reentrant
%option bison-bridge

%{
   #include "parser.tab.h"
   #include "parser.h"

   #define YY_EXTRA_TYPE struct YYExtra*
   #define YY_USER_ACTION yyset_column(yyget_column(yyscanner) + yyget_leng(yyscanner), yyscanner);
%}

%x COMMENTSTATE
%x NATBLKSTATE
%x TXTBLKSTATE
%x MACROSTATE

%%

\/\/[^\n]*                                         { yyset_column(0,yyscanner); }          // single line comment

\/\*                                               { BEGIN(COMMENTSTATE); }                // start a multiline comment
<COMMENTSTATE>\*\/                                 { BEGIN(INITIAL); }                     // end of multiline comment
<COMMENTSTATE>\"(\\[^\n\r]|[^\n\r"\\])*\"          ;                                       // string literal does not end the comment
<COMMENTSTATE>\'(\\[^\n\r]|[^\n\r'\\])*\'          ;                                       // char literal does not end the comment
<COMMENTSTATE>\n                                   { yyset_column(0,yyscanner); }          // reset column
<COMMENTSTATE>.                                    ;                                       // ignore everything in comment

native[ \t]*\{                                     { yyblock_start(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner)); BEGIN(NATBLKSTATE); }
<NATBLKSTATE>\{                                    { yyblock_brackets_inc(yyextra); yyblock_add(yyextra,yytext); }
<NATBLKSTATE>\}                                    { if (yyblock_brackets_dec(yyextra)) { yyblock_add(yyextra,yytext); } else { BEGIN(INITIAL); yylval->token = yyblock_get(yyextra,NATBLOCK); return NAT_BLOCK; } }
<NATBLKSTATE>\"(\\[^\n\r]|[^\n\r"\\])*\"           { yyblock_add(yyextra,yytext); }
<NATBLKSTATE>\'(\\[^\n\r]|[^\n\r'\\])*\'           { yyblock_add(yyextra,yytext); }
<NATBLKSTATE>\n                                    { yyblock_add(yyextra,yytext); yyset_column(0,yyscanner); }
<NATBLKSTATE>.                                     { yyblock_add(yyextra,yytext); }

\"\"\"                                             { yyblock_start(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner)); yyblock_add(yyextra,"\""); BEGIN(TXTBLKSTATE); }
<TXTBLKSTATE>\"\"\"                                { yyblock_add(yyextra,"\""); BEGIN(INITIAL); yylval->token = yyblock_get(yyextra,STRING); return TOKEN; }
<TXTBLKSTATE>\\|\"|\'                              { yyblock_add(yyextra,"\\"); yyblock_add(yyextra,yytext); }
<TXTBLKSTATE>\n                                    { yyblock_add(yyextra,"\\n"); yyset_column(0,yyscanner); }
<TXTBLKSTATE>.                                     { yyblock_add(yyextra,yytext); }

\#[ \t]*\[                                         { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); BEGIN(MACROSTATE); return BLOCK_OPEN; }
\#define[ \t]*\[                                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); BEGIN(MACROSTATE); return BLOCK_OPEN; }
<MACROSTATE>\]                                     { BEGIN(INITIAL); yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_CLOSE; }
<MACROSTATE>[\.0-9a-zA-Z_]*                        { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),IDENT,yytext); return TOKEN; }
<MACROSTATE>[<>!]|==|!=|\<=|\>=                    { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
<MACROSTATE>\|\|                                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; } // || separates alternatives
<MACROSTATE>\&\&                                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return STMT_CLOSE; } // && starts new statement
<MACROSTATE>\(                                     { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; }
<MACROSTATE>\)                                     { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_CLOSE; }
<MACROSTATE>[ \t\r]                                ;
<MACROSTATE>.                                      { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"illegal character",yytext); return TOKEN; }

[ \t\r]                                            ;                                // ignore whitespace
\n                                                 { yyset_column(0,yyscanner); }   // ignore whitespace

\;                                                 { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),SEMICOLON,yytext); BEGIN(INITIAL); return STMT_CLOSE; }
,                                                  { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),COMMA,yytext); return TOKEN; }
as                                                 { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),AS,yytext); return TOKEN; }

\(|\{|\[                                           { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; }
type[ \n\t\r]*\{                                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous struct
struct[ \n\t\r]*\{                                 { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous struct
union[ \n\t\r]*\{                                  { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous union
enum[ \n\t\r]*\{                                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous enum
with[ \n\t\r]*\{                                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // with block
do[ \n\t\r]*\{                                     { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // do block
sizeof[ \n\t\r]*\(                                 { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // sizeof statement
tupleof[ \n\t\r]*\(                                { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous instance
\)|\}|\]                                           { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_CLOSE; }

sizeof                                             { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"sizeof must be followed by round brackets",yytext); return TOKEN; }
tupleof                                            { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"tupleof must be followed by round brackets",yytext); return TOKEN; }

assert                                             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
abort                                              { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
include                                            { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
\@link                                             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
\@export                                           { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
\@optional                                         { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
\@native_name                                      { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
public                                             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
private                                            { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
transparent                                        { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
native|extern|inline|const                         { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
trait|type|struct|union|enum|func|test             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
like|extends                                       { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
augment|with                                       { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
on_init|on_copy|on_move|on_fini|finally            { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
if|unless|else                                     { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
do|while|until|for                                 { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
switch|case|default                                { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
break|continue                                     { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
or|and                                             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),PA_KEY,yytext); return TOKEN; }
return                                             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
\.{3}                                              { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }

static                                             { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"static is not allowed - use either native or with-block instead",yytext); return TOKEN; }
signed                                             { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"signed is not allowed",yytext); return TOKEN; }
unsigned                                           { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"unsigned is not allowed",yytext); return TOKEN; }
register                                           { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"register is not allowed",yytext); return TOKEN; }
volatile                                           { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"volatile is not allowed",yytext); return TOKEN; }
goto                                               { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"goto is not allowed",yytext); return TOKEN; }

null                                               { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),PTR,yytext); return TOKEN; }
true|false                                         { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BOOL,yytext); return TOKEN; }
[0-9]+|0[xX][0-9a-fA-F]+|0[bB][01]+                { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),INT,yytext); return TOKEN; }
([0-9]+|0[xX][0-9a-fA-F]+|0[bB][01]+)[uU]          { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),UINT,yytext); return TOKEN; }
([0-9]+|0[xX][0-9a-fA-F]+|0[bB][01]+)[lL]          { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),LONG,yytext); return TOKEN; }
([0-9]+|0[xX][0-9a-fA-F]+|0[bB][01]+)[uU][lL]      { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),ULONG,yytext); return TOKEN; }
([0-9]+|0[xX][0-9a-fA-F]+|0[bB][01]+)(ll|LL)       { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),LLONG,yytext); return TOKEN; }
([0-9]+|0[xX][0-9a-fA-F]+|0[bB][01]+)[uU](ll|LL)   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),ULLONG,yytext); return TOKEN; }
[0-9]*\.[0-9]+([eE][+-]?[0-9]+)?                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),DOUBLE,yytext); return TOKEN; }
([0-9]*\.[0-9]+([eE][+-]?[0-9]+)?)[fF]             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),FLOAT,yytext); return TOKEN; }
([0-9]*\.[0-9]+([eE][+-]?[0-9]+)?)[lL]             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),LDOUBLE,yytext); return TOKEN; }
\"(\\.|[^\n"\\])*[\n\r]                            { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"illegal newline in string literal",yytext); return TOKEN; }
\"(\\.|[^\n"\\])*\"                                { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),STRING,yytext); return TOKEN; }
\'(\\.|[^\n'\\])                                   { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"illegal character literal",yytext); return TOKEN; }
\'(\\.|[^\n'\\])\'                                 { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),CHAR,yytext); return TOKEN; }

[a-zA-Z_][0-9a-zA-Z_]*                             { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),IDENT,yytext); return TOKEN; }

\$[a-zA-Z_][0-9a-zA-Z_]*                           { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),ENV_STRING,yytext); return TOKEN; }
\#[a-zA-Z_][0-9a-zA-Z_]*                           { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),ENV_NUMBER,yytext); return TOKEN; }

[&|<>=~^!?:*./%+-]                                 { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
\+\+|\-\-|\<\<|\>\>|\&\&|\|\|                      { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
==|!=|\<=|\>=|::                                   { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
\+=|\-=|\*=|\/=|\%=|\<\<=|\>\>=|\&=|\^=|\|=        { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
:base|:disp                                        { yylval->token = new_yytoken(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }

.                                                  { yylval->token = new_yyerror(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),"illegal character",yytext); return TOKEN; }

%%

