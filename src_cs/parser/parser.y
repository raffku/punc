/*
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

%define api.pure full
%lex-param { void* scanner }
%parse-param { struct YYExtra* extra }

%{
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include "parser.h"

   #define YY_EXTRA_TYPE struct YYExtra*
   #define scanner extra->scanner

   extern int yylex();
   extern int yylex_init (void** scn);
   extern int yylex_destroy (void* scn);
   extern void yyset_extra (YY_EXTRA_TYPE user_defined ,void* scn);
   extern void yyset_in (FILE * in_str ,void* scn );

   extern int yyget_lineno (void* yyscanner);
   extern int yyget_column  (void* yyscanner);

   extern int yyparse(struct YYExtra* scn);
   extern void yyerror(struct YYExtra* scn, char* msg);
%}

%code requires {
   struct YYExtra;
   struct YYToken;
   struct Error;
}

%union {
   struct YYToken* token;
}

%token <token> TOKEN
%token <token> BLOCK_OPEN
%token <token> BLOCK_CLOSE
%token <token> STMT_CLOSE
%token <token> NAT_BLOCK

%%

punc:
   entities |  // anything or nothing

entities:
   entity |
   entities entity

entity:
   TOKEN                { insert_yytoken($1); free_yytoken($1); } |
   BLOCK_OPEN           { insert_yytoken($1); free_yytoken($1); } |
   BLOCK_CLOSE          { close_yyblock($1); free_yytoken($1); } |
   STMT_CLOSE           { close_yystmt($1); free_yytoken($1); } |
   NAT_BLOCK            { insert_yytoken($1); close_yystmt($1); free_yytoken($1); }

%%

void punc_parse(FILE* file, struct YYExtra* extra) {
   yylex_init(&scanner);
   yyset_extra(extra,scanner);
   yyset_in(file,scanner);
   do {
      yyparse(extra);
   } while (!feof(file));
   yylex_destroy(scanner);
}

void yyerror(struct YYExtra* extra, char* msg) {
   log_yyerror(extra,yyget_lineno(scanner),yyget_column(scanner),msg);
}
