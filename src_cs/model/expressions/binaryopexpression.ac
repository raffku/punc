/*
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2023 Raffin Kuno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

include "../../basics/location";
include "../../basics/result";
include "../../tools/baseutils";
include "../../tools/expressionutils";
include "../expression";
include "../valuetype";
include "std/string";
include "std/alloc";
include "coll/queue";
include "coll/strings";

include "../object";
include "../objects/variable";

include "std/logger"; Logger log.reg();

public type BinaryOpExpression {
    String operator;
    ExpressionRef left;
    ExpressionRef right;
}

augment ExpressionImpl with StdAlloc;

public Self* BinaryOpExpression::setup(Self* self != null, String operator, ExpressionRef left, ExpressionRef right) {
    assert !self.left.valid() && !self.right.valid();
    self.operator = operator;
    self.left = left;
    self.right = right;
    return self;
}

public augment BinaryOpExpression {
    inline String get_operator(const Self self) { return self.operator; }
    inline void set_operator(Self* self != null, String op) { self.operator = op; }
    inline ExpressionRef* mod_left(Self* self != null) { return &self.left; }
    inline ExpressionRef* mod_right(Self* self != null) { return &self.right; }
}

public augment BinaryOpExpression with ExpressionImpl {
    Queue[ExpressionRefPtr] mod_children(Self* self != null) {
        Queue[ExpressionRefPtr] children;
        children.add(&self.left);
        children.add(&self.right);
        return children;
    }

    Result exp_tree(const Self self, Location location) {
        Result result.msg_fmt(location,"binary op: %s",self.operator.str());
        result.if_msg(self.left.exp_tree());
        result.if_msg(self.right.exp_tree());
        return result;
    }

    String get_display(const Self self) {
        Queue[String] buffer;
        buffer.add(self.left.get_display());
        buffer.add(self.operator);
        buffer.add(self.right.get_display());
        if (self.operator.eqs("[")) buffer.add(String::of("]"));
        return String::join(buffer," ");
    }

    Result calculate_type(const Self self, const Scope scope, Location location, ValueType* vtype != null) {
        Result result;
        ValueType left_type;
        ValueType right_type;
        result.if_msg(self.left.calculate_type(scope,left_type)) and return result;
        result.if_msg(self.right.calculate_type(scope,right_type)) and return result;
        if (math_ops.contains(self.operator)) {
            result.if_msg(self.calculate_math_op_type(scope,location,left_type,right_type,vtype));
        } else if (cmp_ops.contains(self.operator)) {
            result.if_msg(self.calculate_cmp_op_type(scope,location,left_type,right_type,vtype));
        } else if (bit_ops.contains(self.operator)) {
            result.if_msg(self.calculate_bit_op_type(scope,location,left_type,right_type,vtype));
        } else if (assign_ops.contains(self.operator)) {
            // assignments and bit operations have the type of the left operand
            vtype.cpy(right_type).set_disposition(EPHEMERAL);
        } else if (self.operator.eqs("[")) {
            // subscript operation - left type, but persistent and with one less indirection
            if (left_type.get_indirections() < 1) {
                result.msg_fmt(location,"cannot dereference an operand of type %s",left_type.get_display().str());
                return result;
            }
            vtype.cpy(left_type).set_disposition(PERSISTENT).set_indirections(left_type.get_indirections()-1);
        } else if (self.operator.eqs("?")) {
            // ternary op - use right side
            vtype.cpy(right_type);
        } else if (self.operator.eqs(":")) {
            // right side of ternary op - obj types must be equal, so just choose one
            vtype.cpy(left_type);
        } else {
            vtype.set_disposition(INVALID);
            result.msg_fmt(location,"unknown binary operator %s",self.operator.str());
        }
        return result;
    }
}

private Result BinaryOpExpression::calculate_math_op_type(const Self self, const Scope scope, Location location, ValueType left_type, ValueType right_type, ValueType* vtype != null) {
    Result result;
    if (left_type.get_indirections() && right_type.get_indirections()) {
        unless (self.operator.eqs("-")) {
            result.msg_fmt(location,"invalid pointer operation %s",self.get_display().str());
        }
        unless (left_type.type_eq(right_type)) {
            result.msg_fmt(location,"cannot calculate the difference between different pointer types (%s vs. %s)",left_type.get_display().str(),right_type.get_display().str());
        }
        ObjectRef int_type = scope.find_reference(String::of("int"));
        assert int_type.valid();
        vtype.from_object(EPHEMERAL,int_type,false);
    } else if (left_type.get_indirections()) {
        unless (self.operator.eqs("-") || self.operator.eqs("+")) {
            result.msg_fmt(location,"invalid pointer operation %s",self.get_display().str());
        }
        result.if_msg(right_type.check_is_or_declares_integer(self.right.get_location()));
        vtype.cpy(left_type).set_disposition(EPHEMERAL);
    } else {
        // check the operands
        result.if_msg(left_type.check_is_or_declares_numeric(self.left.get_location()));
        result.if_msg(right_type.check_is_or_declares_numeric(self.right.get_location()));
        // get native types
        ValueType native_left_type;
        ValueType native_right_type;
        result.if_msg(left_type.calculate_base_type(native_left_type)) and return result;
        result.if_msg(right_type.calculate_base_type(native_right_type)) and return result;
        // find the bigger type
        ObjectRef left_base_type = native_left_type.get_object_ref();
        ObjectRef right_base_type = native_right_type.get_object_ref();
        int left_index = ordered_primitive_types.index(left_base_type.get_meta().official_name);
        int right_index = ordered_primitive_types.index(right_base_type.get_meta().official_name);
        if (left_index < 0 || right_index < 0) {
            // placeholders, probably; cannot know which one is bigger, so just choose the left one
            vtype.cpy(left_type).set_disposition(EPHEMERAL);
        } else if (left_index < right_index) {
            vtype.cpy(left_type).set_disposition(EPHEMERAL);
        } else {
            vtype.cpy(right_type).set_disposition(EPHEMERAL);
        }
    }
    return result;
}

private Result BinaryOpExpression::calculate_cmp_op_type(const Self self, const Scope scope, Location location, ValueType left_type, ValueType right_type, ValueType* vtype != null) {
    Result result;
    // check
    if (self.operator.eqs("&&") || self.operator.eqs("||")) {
        result.if_msg(left_type.check_is_bool_compatible(self.left.get_location()));
        result.if_msg(right_type.check_is_bool_compatible(self.right.get_location()));
    }
    // TODO more checks
    // return value is always bool
    ObjectRef bool_type = scope.find_reference(String::of("bool"));
    assert bool_type.valid();
    vtype.from_object(EPHEMERAL,bool_type,false);
    return result;
}

private Result BinaryOpExpression::calculate_bit_op_type(const Self self, const Scope scope, Location location, ValueType left_type, ValueType right_type, ValueType* vtype != null) {
    Result result;
    result.if_msg(left_type.check_is_or_declares_integer(self.left.get_location()));
    result.if_msg(right_type.check_is_or_declares_integer(self.right.get_location()));
    vtype.cpy(left_type).set_disposition(EPHEMERAL);
    return result;
}

private Queue[String] math_ops.add_str("+").add_str("-").add_str("*").add_str("/").add_str("%");
private Queue[String] cmp_ops.add_str("<").add_str("<=").add_str(">").add_str(">=").add_str("==").add_str("!=").add_str("&&").add_str("||");
private Queue[String] bit_ops.add_str("&").add_str("^").add_str("|").add_str("<<").add_str(">>");
private Queue[String] assign_ops.add_str("==").add_str("=").add_str("+=").add_str("-=").add_str("*=").add_str("/=").add_str("%=").add_str("<<=").add_str(">>=").add_str("&=").add_str("^=").add_str("|=");

private Queue[String] ordered_primitive_types
    .add_str("ldouble").add_str("double").add_str("float")
    .add_str("ullong").add_str("llong")
    .add_str("ulong").add_str("long")
    .add_str("uint").add_str("int")
    .add_str("ushort").add_str("short")
    .add_str("ubyte").add_str("byte")
    .add_str("char").add_str("bool");
