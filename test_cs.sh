#! /bin/bash

DIR=`pwd`

export PUNC_EXEC_PATH=$DIR/bootstrap_phase2/punc
export PUNC_INCLUDE_PATH=$DIR/lib

cd build_cs
rm *.c *.h build_cs
cp ../src_cs/parser/parser.y ../src_cs/parser/parser.l .

$PUNC_EXEC_PATH --inlines=2 --verbose=1 --test=project --out=punc_test.c ../src_cs/parser/modelbuilder.ac || exit 1
#$PUNC_EXEC_PATH --inlines=2 --verbose=1 --test=project --out=punc_test.c ../src_cs/parser/loader.ac ../src_cs/parser/modelbuilder.ac || exit 1
#$PUNC_EXEC_PATH --inlines=2 --verbose=1 --test=project --assert=crash --out=punc_test.c ../src_cs/parser/loader.ac ../src_cs/parser/modelbuilder.ac || exit 1

bison -d parser.y || exit 1
flex -o parser.yy.c parser.l || exit 1
gcc -g -Wall -Wimplicit-function-declaration *.c -o punc_test -lpthread || exit 1
valgrind --tool=memcheck --leak-check=full --num-callers=40 ./punc_test
echo

cd ..

