#! /bin/bash

DIR=`pwd`

export PUNC_INCLUDE_PATH=$DIR/lib

echo "--- phase 1 ---"

cd bootstrap_phase1
cp ../src/punc.y ../src/punc.l .

bison -d punc.y || exit 1
flex -o punc.yy.c punc.l || exit 1
gcc -g -Wimplicit-function-declaration *.c -o punc || exit 1

echo "--- phase 2 ---"

cd ../bootstrap_phase2
rm *.c *.h punc
cp ../src/punc.y ../src/punc.l .

time ../bootstrap_phase1/punc --inlines=5 --verbose=1 --release ../src/main/main.ac || exit 1
bison -d punc.y || exit 1
flex -o punc.yy.c punc.l || exit 1
gcc -g -O3 *.c -o punc || exit 1

cd ..

