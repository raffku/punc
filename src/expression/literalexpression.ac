/* 
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

include "../common";

Logger log.tag("LiteralExpression").reg();

public transparent type LiteralExpression {
   TypeSpec typ;
   String text;
   bool lvalue = false; // only for internal use, no real literal can be assignable
on_copy(self,other):
   self.typ = other.typ;
   self.text = other.text;
   self.lvalue = other.lvalue;
}

public augment LiteralExpression with ExpressionImpl {
   uint get_prio { return 0; }
   
   bool add_left {
      log.error("%s:%d,%d - trying to add subexpression left of literal",other.module.get_name().str(),other.lineno,other.column);
      return false;
   }
   
   bool add_right {
      if (self.text.starts_with("\"")) {
         if (LiteralExpression* other_lit = other.get[LiteralExpression]()) {
            if (other_lit.text.starts_with("\"")) {
               String new_text.fmt("%s%s",self.text.str(),other_lit.text.str());
               self.text = new_text;
               other.free(1);
               return true;
            }
         }
      }
      log.error("%s:%d,%d - trying to add subexpression right of literal",other.module.get_name().str(),other.lineno,other.column);
      return false;
   }
   
   void clone_into {
      auto trg = target.impl.new[Self]();
      unless (self.typ.nil()) trg.typ.clone(self.typ);
      trg.text = self.text;
      trg.lvalue = self.lvalue;
   }
   
   void substitute {
      unless (self.typ.nil()) self.typ.substitute(orig,subst);
   }
   
   bool evaluate {
      return true; // nothing to do
   }

   bool adapt_code {
      return true; // nothing to do
   }
   
   bool inject_type {
      if (self.typ.can_assign_to(target_type,null)) {
         return true;
      } else {
         String actual_display = self.typ.get_display(null,false);
         String target_display = target_type.get_display(null,false);
         log.error("%s:%d,%d - cannot adapt %s to %s",sub.module.get_name().str(),sub.lineno,sub.column,actual_display.str(),target_display.str());
         return false;
      }
   }
   
   TypeSpec get_type {
      unless (self.typ.nil()) {
         return self.typ;
      } else {
         // for internal purposes i sometimes misuse this expression type
         TypeSpec result.new_at(sub.module,sub.lineno,sub.column);
         return result;
      }
   }
   
   TypeSpec get_called_type {
      /*log.error("%s:%d,%d - cannot call a literal",sub.module.get_name().str(),sub.lineno,sub.column);
      VariableOld result;
      result.module = sub.module;
      result.lineno = sub.lineno;
      result.column = sub.column;
      return result;*/
      if (ellipsis) *ellipsis = true;   // because I am lazy when generating code, a couple of times - TODO be less lazy and return error again
      return self.get_type(sub); // because I am lazy when generating code, a couple of times - TODO be less lazy and return error again
   }
   
   /*void bind_from_map {
      LiteralExpression* result = impl.new[LiteralExpression]();
      *result = *self;
   }*/
   
   void get_dependencies { }
   
   bool is_compile_const {
      return true;
   }

   Artifact get_artifact {
      return Artifact nil;
   }
   
   void add_output_string {
      if (self.text.eqs("null")) output.new().set("(void*)0");
      else if (self.text.eqs("false")) output.new().set("0");
      else if (self.text.eqs("true")) output.new().set("1");
      else output.new().cpy(self.text);
   }
}
