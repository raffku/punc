/* 
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

include "../common";

Logger log.tag("FncArtifact").reg();

public type FncArtifact {
   Function fnc;
}

public augment FncArtifact with ArtifactImpl {
   Function* get_function(FncArtifact* self) { return &self.fnc; }

   void resolve(FncArtifact* self, Artifact art) {
      self.fnc.resolve();
   }

   void register_ctx(FncArtifact* self, Artifact art) {
      self.fnc.register_ctx();
   }
   
   void bind(FncArtifact* self, Artifact bound_art) {
      FncArtifact* result = bound_art.new[FncArtifact]();
      //result.fnc.create_bound_from(self.fnc,bound_art,bound_art.get_ctx(),bound_art.get_map());
      result.fnc.clone(self.fnc,bound_art.get_ctx());
      bound_art.get_map().mod_each(result.fnc.get_body(),map_op {
         obj.substitute(tmpl_art,type_art);
      });
      result.fnc.register_ctx();
      result.fnc.set_name(bound_art.get_out_name());
   }
   
   void substitute {
      self.fnc.substitute(orig,subst);
   }
   
   bool evaluate_code(FncArtifact* self, Artifact art) {
      self.fnc.evaluate_code();
      return true;
   }
   
   bool adapt_code(FncArtifact* self, Artifact art) {
      return self.fnc.adapt_code();
   }
   
   void get_dependencies(Self* self, Artifact art, int detail, Artifacts* exst, Artifacts* cmpl) {
      add_fnc_dependency(self.fnc,detail,exst,cmpl);
   }
   
   auto add_prep_string       { assert; }
   auto is_default_copy_type  { assert; }
   auto get_type_fnc          { return null; }

   // detail: 0...pre-declaration, 1...declaration, 3...definition
   void write(FncArtifact* self, Artifact art, Queue[String]* output, int detail) {
      if (self.fnc.writable_for(detail)) {
         output.new().set("\n");
         self.fnc.write(art,output,detail);
      }
   }
}
