/* 
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

include "../common";
include "../expression/load";
include "../util/variable_helper";

Logger log.tag("variable/variable").reg();

public transparent type Variable {
   TypeSpec typ;
   String name;
   
   Expression prep_exp;
   Command prep_cmd;
   bool is_member = false;          // member of a compound type -> prep command cannot be directly used, just stored

   bool return_value = false;       // this variable is used as a return value, must not be fini'd
   bool only_copy = false;          // need no init for this variable, because it will be copied immediately
   bool only_fini = false;          // need only fini for this variable (no prep, init, copy or move)
   bool marked = false;
   
   Artifact counter_art;            // call counter to track if variable was passed into function which will fini it
}

public augment Variable {
   void resolve(Variable* self != null, Context ctx) {
      // resolve type
      self.typ.resolve(ctx);
      // resolve prep expression
      unless (self.prep_exp.nil()) {
         self.prep_exp.load(self.typ.get_artspec().get_module(),ctx,false);
      }
      // resolve prep command
      unless (self.is_member || self.prep_cmd.nil()) {
         self.prep_cmd.resolve();
      }
   }

   void clone(Variable* self, const Variable* other, Context parent_ctx) {
      self.typ.clone(other.typ);
      self.name = other.name;
      self.is_member = other.is_member;
      self.return_value = other.return_value;
      unless (other.prep_exp.nil()) {
         self.prep_exp.clone(other.prep_exp);
      }
      unless (other.prep_cmd.nil()) {
         self.prep_cmd.clone(other.prep_cmd,parent_ctx);
      }
      self.only_copy = other.only_copy;
      // only_fini, marked and counter_art not copied over
   }
   
   void substitute(Variable* self != null, const Artifact orig, const Artifact subst) {
      if (self.name.eq(orig.get_name())) {
         self.name = subst.get_name();
      }
      self.typ.substitute(orig,subst);
      unless (self.prep_exp.nil()) {
         self.prep_exp.substitute(orig,subst);
      }
      unless (self.prep_cmd.nil()) {
         self.prep_cmd.substitute(orig,subst);
      }
      // must have happened before evaluate, so counter art must be nil
      assert self.counter_art.nil();
   }
   
   void register_ctx(Variable* self != null, Context ctx) {
      self.typ.register_ctx(ctx);
   }

   void evaluate_code(Variable* self != null, Context ctx) {
      // evaluate prep expression
      unless (self.prep_exp.nil()) {
         self.prep_exp.evaluate(ctx);
      }
      // resolve auto type name
      if (self.typ.get_artspec().get_art_name().eqs("auto")) {
         //log.info("%s:%d,%d - resolve auto from %d",self.typ.get_artspec().get_module().get_name().str(),self.typ.get_artspec().get_lineno(),self.typ.get_artspec().get_column(),self.prep_exp.nil());
         bool type_resolved = false;
         if (self.prep_exp.nil()) {
            log.error("%s:%d,%d - auto type without expression",self.typ.get_artspec().get_module().get_name().str(),self.typ.get_artspec().get_lineno(),self.typ.get_artspec().get_column());
         } else {
            TypeSpec exp_type = self.prep_exp.get_type();
            unless (exp_type.nil()) {
               self.typ = exp_type;
               type_resolved = true;
            }
         }
         unless (type_resolved) {
            log.error("%s:%d,%d - cannot determine auto type from expression",self.typ.get_artspec().get_module().get_name().str(),self.typ.get_artspec().get_lineno(),self.typ.get_artspec().get_column());
         }
      }
      // evaluate type
      self.typ.register_ctx(ctx);
      self.typ.evaluate_code(ctx);
      // evaluate prep command
      unless (self.is_member || self.prep_cmd.nil()) {
         self.prep_cmd.evaluate_code();
      }
      // check prep_exp constness
      unless (self.prep_exp.nil()) {
         TypeSpec prep_type = self.prep_exp.get_type();
         unless (self.typ.preserves_constness(prep_type.is_const())) {
            log.error("%s:%d,%d - cannot assign %s to a non-const variable",
                      self.typ.get_artspec().get_module().get_name().str(),self.typ.get_artspec().get_lineno(),self.typ.get_artspec().get_column(),prep_type.get_display(null,false).str());
         }
      }
   }
   
   bool adapt_code(Variable* self != null, Artifact art, Context ctx) {
      bool ok = true;
      // check assignment has correct type
      unless (self.prep_exp.nil()) {         
         TypeSpec var_type = self.typ;
         TypeSpec exp_type = self.prep_exp.get_type();
         int delta_stars = 0;
         if (!exp_type.can_assign_to(var_type,delta_stars)) {
            String var_disp = var_type.get_display(null,false);
            String exp_disp = exp_type.get_display(null,false);
            log.error("%s:%d,%d - cannot initialize %s from %s",self.typ.get_artspec().get_module().get_name().str(),self.typ.get_artspec().get_lineno(),self.typ.get_artspec().get_column(),var_disp.str(),exp_disp.str());
            ok = false;
         } else if (delta_stars != 0) {
            String var_disp = var_type.get_display(null,false);
            String exp_disp = exp_type.get_display(null,false);
            log.error("%s:%d,%d - cannot initialize %s from %s",self.typ.get_artspec().get_module().get_name().str(),self.typ.get_artspec().get_lineno(),self.typ.get_artspec().get_column(),var_disp.str(),exp_disp.str());
            ok = false;
         }
      }
      // adapt commands
      if (!self.only_fini) {
         ok = art.add_var_prep_commands(ctx,ctx.get_pre_cmds(),false) && ok;
         ok = self.adapt_prep_exp(ctx) && ok;
         ok = self.adapt_prep_cmd(ctx) && ok;
      }
      if (!self.return_value) {
         ok = art.add_var_disp_commands(ctx,ctx.get_post_cmds()) && ok;
      }
      return ok;
   }
}

