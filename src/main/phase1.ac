/* 
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * At the end of phase 1:
 * - Given and included modules are parsed
 * - Provided environment variables are set and optional code paths are handled
 * - Artifacts, including commands and expressions, are created
 * - No templates are yet bound
*/

include "../common";

uint subphase = 0;
Logger out.tag("main").reg();

public bool phase1(int argc, char** argv) {
   Queue[String] files;
   parse_cmdline(argc,argv,files) or {
      show_help(argv[0]);
      return false;
   }
   create_builtins() or return false;
   read_files(files) or return false;
   resolve_provides() or return false;
   load_artifacts(files) or return false;
   collect_artifacts() or return false;
   punc_artifacts_collected = true;
   resolve_prototypes() or return false;
   resolve_artifacts() or return false;
   return true;
}

bool parse_cmdline(int argc, char** argv, Queue[String]* files) {
   bool help = false;
   for (int i=1; i<argc; i++) {
      String param.set(argv[i]);
      if (param.eqs("-h")) {
         help = true;
      } else if (param.eqs("--help")) {
         help = true;
      } else if (param.eqs("-v")) {
         punc_verbosity_level = 2;
      } else if (param.eqs("--verbose")) {
         punc_verbosity_level = 2;
      } else if (param.starts_with("-v")) {
         String verbosity.slice(param,2,-1);
         punc_verbosity_level = atoi(verbosity.str());
      } else if (param.starts_with("--verbose=")) {
         String verbosity.slice(param,10,-1);
         punc_verbosity_level = atoi(verbosity.str());
      } else if (param.eqs("-t")) {
         if (punc_test_level < 1) punc_test_level = 1;
      } else if (param.eqs("--test")) {
         if (punc_test_level < 1) punc_test_level = 1;
      } else if (param.eqs("-tf")) {
         if (punc_test_level < 1) punc_test_level = 1;
      } else if (param.eqs("--test=files")) {
         if (punc_test_level < 1) punc_test_level = 1;
      } else if (param.eqs("-tp")) {
         if (punc_test_level < 2) punc_test_level = 2;
      } else if (param.eqs("--test=project")) {
         if (punc_test_level < 2) punc_test_level = 2;
      } else if (param.eqs("-ta")) {
         if (punc_test_level < 3) punc_test_level = 3;
      } else if (param.eqs("--test=all")) {
         if (punc_test_level < 3) punc_test_level = 3;
      } else if (param.eqs("-r")) {
         punc_assert_level = 0;
      } else if (param.eqs("--release")) {
         punc_assert_level = 0;
      } else if (param.eqs("--assert=none")) {
         punc_assert_level = 0;
      } else if (param.eqs("-aw")) {
         punc_assert_level = 1;
      } else if (param.eqs("--assert=warn")) {
         punc_assert_level = 1;
      } else if (param.eqs("-ae")) {
         punc_assert_level = 2;
      } else if (param.eqs("--assert=exit")) {
         punc_assert_level = 2;
      } else if (param.eqs("-ac")) {
         punc_assert_level = 3;
      } else if (param.eqs("--assert=crash")) {
         punc_assert_level = 3;
      } else if (param.eqs("-in")) {
         punc_inline_level = -1;
      } else if (param.eqs("--inlines=none")) {
         punc_inline_level = -1;
      } else if (param.eqs("-ie")) {
         punc_inline_level = 0;
      } else if (param.eqs("--inlines=explicit")) {
         punc_inline_level = 0;
      } else if (param.starts_with("-i")) {
         String inl.slice(param,2,-1);
         punc_inline_level = atoi(inl.str());
      } else if (param.starts_with("--inlines=")) {
         String inl.slice(param,10,-1);
         punc_inline_level = atoi(inl.str());
      } else if (param.starts_with("--project_root=")) {
         punc_project_root.slice(param,15,-1);
      } else if (param.starts_with("--project_name=")) {
         punc_project_name.slice(param,15,-1);
      } else if (param.starts_with("--out=")) {
         punc_output_file.slice(param,6,-1);
      } else if (param.starts_with("-")) {
         printf("unknown parameter %s\n\n",param.str());
         help = true;
      } else {
         files.new().cpy(param);
      }
   }   
   // prepare environment
   unless (punc_project_name.str()) {
      punc_project_name.set("pnc");
   }
   if (punc_assert_level < 0) {
      punc_assert_level = (punc_test_level>0)?1:2;
   }
   switch (punc_verbosity_level) {
      case 0: Logger::set_threshold(warn); break;
      case 1: Logger::set_threshold(info); break;
      case 2: Logger::set_threshold(debug); break;
   }   
   if (punc_test_level > 0) {
      String test_level.fmt("%d",punc_test_level);
      setenv("PUNC_TEST",test_level.str(),true);
   }
   if (punc_assert_level == 0) {
      setenv("PUNC_RELEASE","1",true);
   }
   // done
   return files.size() > 0 && !help;
}

void show_help(const char* executable) {
   printf("PuNC's Not C\n");
   printf("Synopsis: %s [options] file [file]*\n",executable);
   printf("Options:\n");
   printf("--help, -h: Show this help\n");
   printf("--verbose, -v: Set verbosity to 2 (default 1)\n");
   printf("--verbose=<level>, -v<level>: Set verbosity to 0-2 (default 1)\n");
   printf("--test, -t: Compile tests instead of real program\n");
   printf("--test=files, -tf: Same as above, compile tests for directly given files only\n");
   printf("--test=project, -tp: Compile tests for the entire project\n");
   printf("--test=all, -ta: Compile tests for the libraries, too\n");
   printf("--assert=none, --release, -r: Compile without assertions\n");
   printf("--assert=warn, -aw: Assert by printing a warning (default with test compiles)\n");
   printf("--assert=exit, -ae: Assert by exiting the program (default with regular compiles)\n");
   printf("--assert=crash, -ac: Assert by segfaulting\n");
   printf("--inlines=none, -in: Do not generate inline functions\n");
   printf("--inlines=explicit, -ie: Generate explicit inline functions\n");
   printf("--inlines=[1-10], -i[1-10]: Generate explicit inline functions and generated functions with [1-10] commands in them\n");
   printf("--project_root=[dirname]: Explicitly set the source code root if not inferrable from the input files on the command line\n");
   printf("--project_name=[name]: Use this string as the project name for e.g. global init/fini code\n");
   printf("--out=[filename]: Generate a monolithic C file\n");
   printf("\n");
}

public bool create_builtins() {
   out.info("[1.%02u] Preparing builtins... ",++subphase);
   punc_main_module = Module::create(punc_project_name,String::of(""),4) or assert;
      
   // builtin types
   create_builtin_self();
   create_builtin_type("auto",null,"auto",false,false,false);
   create_builtin_type("void",null,"void",false,false,false);
   create_builtin_type("bool",null,"int",true,false,false);
   create_builtin_type("char",null,"char",true,true,true);
   create_builtin_type("byte","signed","char",true,true,true);
   create_builtin_type("short",null,"short",true,true,true);
   create_builtin_type("int",null,"int",true,true,true);
   create_builtin_type("long",null,"long",true,true,true);
   create_builtin_type("llong",null,"long long",true,true,true);
   create_builtin_type("ubyte","unsigned","char",true,true,true);
   create_builtin_type("ushort","unsigned","short",true,true,true);
   create_builtin_type("uint","unsigned","int",true,true,true);
   create_builtin_type("ulong","unsigned","long",true,true,true);
   create_builtin_type("ullong","unsigned","long long",true,true,true);
   create_builtin_type("float",null,"float",true,true,false);
   create_builtin_type("double",null,"double",true,true,false);
   
   // func TestCase
   TypeSpec testcase_base.create_for_builtin(0,false,"void");
   punc_testcase_func = create_builtin_func(testcase_base,"TestCase");

   // trait Allocatable
   Artifact alloc = create_builtin_trait("Allocatable");
   TraitType* alloc_trait = alloc.get[TraitType]();
   
   Queue[Parameter] alloc_params.new().create_for_builtin(0,false,"uint","count");
   TypeSpec alloc_base.create_for_builtin(1,false,"Self");
   alloc_trait.new_member().create_for_builtin(alloc.get_ctx(),alloc_base,"alloc",alloc_params);
   
   Queue[Parameter] free_params;
   free_params.new().create_for_builtin(1,false,"Self","ptr");;
   free_params.new().create_for_builtin(0,false,"uint","count");
   TypeSpec free_base.create_for_builtin(0,false,"void");
   alloc_trait.new_member().create_for_builtin(alloc.get_ctx(),free_base,"free",free_params);
   
   out.info("(done)"); // subtract main module
   return true;
}

void create_builtin_self() {
   String empty;
   String art_name.set("Self");
   Artifact art = punc_main_module.new_artifact(0,0,true,wellknown,null,art_name,empty,art_name);
   art.set_evaluated(true);
   art.new[SelfType]();
}

void create_builtin_type(const char* name, const char* nat_prefix, const char* nat_name, bool instantiable, bool number, bool integer) {
   // prepare artifact
   String art_name.set(name);
   String art_prefix;
   if (nat_prefix) art_prefix.set(nat_prefix);
   String art_out_name.set(nat_name);
   Artifact art = punc_main_module.new_artifact(0,0,true,wellknown,null,art_name,art_prefix,art_out_name);
   art.set_evaluated(true);
   // prepare impl
   VoidType* impl = art.new[VoidType]();
   impl.instantiable = instantiable;
   impl.number = number;
   impl.integer = integer;
}

Artifact create_builtin_func(TypeSpec typ, const char* name) {
   // prepare artifact
   String empty;
   String art_name.set(name);
   Artifact art = punc_main_module.new_artifact(0,0,false,wellknown,null,art_name,empty,art_name);
   art.set_evaluated(true);
   // prepare impl
   FuncType* fnc = art.new[FuncType]();
   fnc.function.init(typ,art.get_name(),art.get_ctx());
   return art;
}

Artifact create_builtin_trait(const char* name) {
   // prepare artifact
   String art_name.set(name);
   String art_out_name.fmt("struct %s",name);
   Artifact art = punc_main_module.new_artifact(0,0,false,wellknown,null,art_name,String::of("struct"),art_name);
   art.set_evaluated(true);
   // prepare impl
   art.new[TraitType]();
   return art;
}

void Function::create_for_builtin(Function* self, Context parent_ctx, TypeSpec base, const char* name, Queue[Parameter]* params) {
   String name_string.set(name);
   self.init(base,name_string,parent_ctx);
   self.get_parameters().swap(params);
}

void Parameter::create_for_builtin(Parameter* self, uint stars, bool cnst, const char* type_name, const char* var_name) {
   self.typ.new_at(punc_main_module,0,0);
   self.typ.add_stars(stars);
   self.typ.set_constness(cnst);
   self.typ.mod_artspec().set_art_name(String::of(type_name));
   self.name.set(var_name);
}

void TypeSpec::create_for_builtin(TypeSpec* self, uint stars, bool cnst, const char* type_name) {
   self.new_at(punc_main_module,0,0);
   self.add_stars(stars);
   self.set_constness(cnst);
   self.mod_artspec().set_art_name(String::of(type_name));
}

include "../util/fsutil";
include "../module/load";
include "../module/preartifact";

bool read_files(const Queue[String]* files) {
   out.info("[1.%02u] Reading files... ",++subphase);
   String dirname.set(".");
   unless (punc_project_root.str()) {
      punc_project_root = files.extract_common_path();
      punc_project_root.append_str("/");
   }
   for (QueueIter[String] file_iter.start(files); file_iter.get(); file_iter.next()) {
      Module::read_file(1,dirname,file_iter.get(),punc_project_root);
   }
   out.info("(%u files)",Module::count_modules()-1); // subtract builtin module   
   return !Logger::flush_errors();
}

bool resolve_provides() {
   out.info("[1.%02u] Setting environment variables... ",++subphase);
   uint count = 0;
   Module::mod_each(structof (&count as count),op(module,ctx) {
      *ctx.count += module.get_provides().size();
      module.apply_provides();
   });
   out.info("(%u variables)",count);
   return !Logger::flush_errors();
}

bool load_artifacts(const Queue[String]* files) {
   out.info("[1.%02u] Loading artifacts... ",++subphase);
   String dirname.set(".");
   for (QueueIter[String] file_iter.start(files); file_iter.get(); file_iter.next()) {
      Module::load_artifacts(1,dirname,file_iter.get(),punc_project_root);
   }
   uint count = 0;
   Module::mod_each(structof (&count as count),op(module,ctx) {
      *ctx.count += module.count_artifacts();
   });   
   out.info("(%u artifacts)",count);
   return !Logger::flush_errors();
}

bool collect_artifacts() {
   out.info("[1.%02u] Registering artifacts... ",++subphase);
   uint count = 0;
   Module::mod_each(structof (&count as count), op(mod,ctx) {
      *ctx.count += mod.register_artifacts();   
   });
   out.info("(%u registered)",count);   
   return !Logger::flush_errors();
}

bool resolve_prototypes() {
   out.info("[1.%02u] Resolving function prototypes... ",++subphase);
   uint count_funcs = 0;
   uint count_traits = 0;
   Module::mod_each(structof (&count_funcs as count_funcs; &count_traits as count_traits), op {
      unless (Logger::pending_errors()) {
         obj.mod_each_artifact(ctx, op {
            if (obj.is[FuncType]()) {
               obj.resolve();
               ++(*ctx.count_funcs)
            } else if (obj.is[TraitType]()) {
               obj.resolve();
               ++(*ctx.count_traits)
            }
         });
      }
   });
   out.info("(%u funcs, %u traits)",count_funcs,count_traits);   
   return !Logger::flush_errors();
}

bool resolve_artifacts() {
   out.info("[1.%02u] Resolving other artifacts... ",++subphase);
   uint count = 0;
   Module::mod_each(structof (&count as count), op {
      unless (Logger::pending_errors()) {
         obj.mod_each_artifact(ctx, op {
            unless (obj.is[FuncType]() || obj.is[TraitType]()) {
               obj.resolve();
               ++(*ctx.count)
            }
         });
      }
   });
   out.info("(%u artifacts)",count);   
   return !Logger::flush_errors();
}

/////////////////////

String Queue[String]::extract_common_path(const Queue[String]* files != null) {
   String prefix;
   files.with_each(prefix, op {
      File file.stat(ctx.str()) or return;
      if (file.is_reg()) {
         String realfile;
         realpath(ctx.str(),realfile.new(PATH_MAX));
         String realdir.set(dirname(realfile.mod()));
         if (obj.len() == 0) {
            obj.cpy(realdir);
         } else if (!realdir.starts_with(obj.str())) {
            const char* file = realdir.str();
            char* prefix = obj.mod();
            while (*prefix && *prefix == *file) {
               prefix++;
               file++;
            }
            *prefix = '\0';
         }
      }
   });
   return prefix;
}

Module Module::read_file(uint level, String dir, String file, String rootpath) {
   String fullpath = get_source_path(dir,file,&rootpath,&level);
   unless(fullpath.str()) return Module no_module;
   if (Module found = Module::get(fullpath)) {
      if (found.get_level() > level) found.set_level(level);
      return found;
   } else {
      Module mod = Module::create(fullpath,rootpath,level);
      mod.from_file(fullpath.str());
      // load included modules
      if (mod && !Logger::pending_errors()) {
         Queue[String] includes;
         mod.peek_includes(includes);
         String fullpath_cpy = fullpath;
         String mod_dirname.set(dirname(fullpath_cpy.mod()));
         int sublevel = (level == 1)?2:level;
         while (auto inc = includes.pop()) {
            Module::read_file(sublevel,mod_dirname,inc,rootpath);
         }
      }
      // done
      return mod;
   }
}

void Module::from_file(Module self, const char* filename) {
   out.debug("Reading %s",filename);
   Queue[Statement] statements;
   parse_file(filename,statements);
   self.extract_prearts(statements);
}

void Module::apply_provides(Module self) {
   for (auto iter = self.get_provides().iter(); iter.get(); iter.next()) {
      setenv(iter.get().str(),self.get_fullpath().str(),true) and {
         out.error("%s: failed to set environment variable %s = %s",self.get_name().str(),iter.get().str(),self.get_fullpath().str());
      }
   }
}

Module Module::load_artifacts(uint level, String dir, String file, String rootpath) {
   Module mod = Module::read_file(level,dir,file,rootpath);
   if (mod) {
      unless (mod.is_loaded()) {
         mod.set_loaded();
         mod.apply_env_variables();
         mod.parse_prearts();
         mod.mod_each_include(mod,op {
            // mod is actually ignored, but i need some input for context
            obj.load_artifacts();
         });
         // do the same for the test module
         if (Module test_mod = Module::get(mod.get_public_test_module_name())) {
            test_mod.mod_each_include(test_mod,op {
               // mod is actually ignored, but i need some input for context
               obj.load_artifacts();
            });
         }
      }
   }
   return mod;
}

void Include::load_artifacts(Include self != null) {
   unless (self.get_nat()) {
      String filecpy = self.get_module().get_fullpath();
      String dir.set(dirname(filecpy.mod()));
      for (auto file = self.get_files().iter(); file.get(); file.next()) {
         Module module = Module::load_artifacts((self.get_module().get_level()==1)?2:self.get_module().get_level(),dir,file.get(),self.get_module().get_rootpath());
         if (module) {
            self.add_module(module);
         } else {
            out.error("%s:%d,%d - cannot parse module %s",self.get_module().get_name().str(),self.get_lineno(),self.get_column(),file.get().str());
         }
      }
   }
}

uint Module::register_artifacts(Module self != null) {
   out.debug("Registering artifacts in %s",self.get_name().str());
   // collect modules
   Set[Module,Module::cmp] modules;
   self.collect_modules(modules);
   // register artifacts
   uint total = 0;
   total += self.register_own_artifacts(); // first register own artifacts
   for (auto modptr = modules.sweep().iter(); modptr.get(); modptr.next()) {
      unless (self == *modptr.get()) total += self.register_foreign_artifacts(modptr.get());
   }
   return total;
}   


void Module::collect_modules(Module self, Set[Module,Module::cmp]* modules != null) {
   *modules.new() = punc_main_module;
   *modules.new() = self;
   self.mod_each_include(structof (modules as modules),op {
      obj.collect_modules(ctx.modules);
   });
}

uint Module::register_own_artifacts(Module self) {
   uint total = 0;
   self.mod_each_artifact(structof (self as self; &total as total),op {
      Artifact art = *obj;
      ctx.self.register_artifact(art);
      ++(*ctx.total);
   });
   return total;
}

uint Module::register_foreign_artifacts(Module self, Module holder) {
   uint total = 0;
   holder.mod_each_artifact(structof (self as self; &total as total),op {
      Artifact art = *obj;
      if (art.get_vis() >= pub) {
         ctx.self.register_artifact(art);
         ++(*ctx.total);
      }
   });
   return total;
}

void Module::register_artifact(Module self, Artifact art) {
   Artifact registered = self.get_ctx().add_artifact(art);
   unless (registered.eq(art)) {
      if (art.get_module() == registered.get_module() || registered.get_vis() >= pub) { // otherwise i am fine, private artifacts take precedence
         out.error("%s - conflicting artifacts with name %s within this context",self.get_name().str(),art.get_name().str());
         out.error("%s:%d,%d - registered artifact",registered.get_module().get_name().str(),registered.get_lineno(),registered.get_column());
         out.error("%s:%d,%d - conflicting artifact",art.get_module().get_name().str(),art.get_lineno(),art.get_column());
      }
   }
}

void Include::collect_modules(Include self, Set[Module,Module::cmp]* modules) {
   for (auto iter = self.get_modules().iter(); iter.get(); iter.next()) {
      *modules.new() = *iter.get();
      unless (modules.ins()) {
         iter.get().collect_included_modules(modules);
      }
   }
}

void Module::collect_included_modules(Module self, Set[Module,Module::cmp]* modules) {
   self.mod_each_include(structof (modules as modules),op {
      if (obj.get_vis() >= pub) obj.collect_modules(ctx.modules);
   });
}

native include "limits.h", "stdlib.h", "libgen.h" {
   const uint PATH_MAX;
   int atoi(const char* str);
   char *realpath(const char *path, char *resolved_path);
   char *dirname(char *path);
}


