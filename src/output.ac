/* 
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Log is used for all output of punc:
 * - trace is reserved for development of punc itself
 * - debug, info are normal output, of verbosity 2 and 1 respectively
 * - error and warn are not output directly, but stored, and will be flushed by dedicated functions
 * - fatal is an internal error and will terminate the program
 * - only every second info() for the topic "Main" will have a newline - except some other output interrupts
 */

public include "std/logger";

public augment Logger {
   inline uint pending_errors() { return errors.size(); }
   
   bool flush_errors() {
      if (errors.size() > 0) {
         if (main_info_written && main_info_cnt % 2) fprintf(stdout,"\n");
         while (auto err = errors.pop()) {
            fprintf(stderr,"%s\n",err.str());
         }
         main_info_written = false;
         return true;
      }
      return false;
   }
}

/////////////////////////////

#provide[PUNC_LOGGER_WRITE_OVERRIDE]

private Queue[String] warnings;
private Queue[String] errors;

private String main_topic.set("main");
private uint main_info_cnt = 0;
private bool main_info_written = false;

@export
public void _logger_write(const Logger logger, Severity severity, const char* fmt, ...) {
   // decide where to put the output
   String* message = null;
   FILE* stream = null;
   if (severity == warn) {
      message = warnings.new();
   } else if (severity == error) {
      message = errors.new();
   } else {
      stream = (severity < warn)?stdout:stderr;
   }
   // print the message
   va_list args;
   va_start(args,fmt);
   if (message) {
      message.vfmt(fmt,args);
   } else if (severity == info && logger.has_tag("main")) {
      vfprintf(stream,fmt,args);
      if ((main_info_cnt++) % 2) fprintf(stream,"\n");
      else fflush(stream);
      main_info_written = true;
   } else {
      if (main_info_written && main_info_cnt % 2) fprintf(stream,"\n");
      vfprintf(stream,fmt,args);
      fprintf(stream,"\n");
      main_info_written = false;
   }
   va_end(args);
   // for fatal errors, exit the program
   if (severity == fatal) exit(1);
}

include "coll/queue";
include "std/string";

native include "stdarg.h", "stdio.h" {
   type va_list;
   type FILE;
   type size_t like uint;
      
   FILE* stdout;
   FILE* stderr;
      
   void va_start(va_list args, const void* first);
   void va_end(va_list args);
   int fprintf(FILE *stream, const char *format, ...);
   int vfprintf(FILE *stream, const char *format, va_list ap);
   int fflush(FILE* stream);
   
   void exit(int status);
}
