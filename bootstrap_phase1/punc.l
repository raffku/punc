/* 
 * This file is part of the PUNC distribution (https://gitlab.com/raffku/punc).
 * Copyright (c) 2020 Raffin Kuno.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

%option noyywrap
%option yylineno
%option reentrant
%option bison-bridge

%{
   #include "punc.tab.h"
   #include "parser.h"

   #define YY_EXTRA_TYPE struct ParseContext*
   #define YY_USER_ACTION yyset_column(yyget_column(yyscanner) + yyget_leng(yyscanner), yyscanner);
%}

%x COMMENTSTATE
%x NATBLKSTATE
%x TXTBLKSTATE

%%

\/\/[^\n]*                                      { yyset_column(0,yyscanner); }          // single line comment

\/\*                                            { BEGIN(COMMENTSTATE); }                // start a multiline comment
<COMMENTSTATE>\*\/                              { BEGIN(INITIAL); }                     // end of multiline comment
<COMMENTSTATE>\"(\\[^\n\r]|[^\n\r"\\])*\"       ;                                       // string literal does not end the comment
<COMMENTSTATE>\'(\\[^\n\r]|[^\n\r'\\])*\'       ;                                       // char literal does not end the comment
<COMMENTSTATE>\n                                { yyset_column(0,yyscanner); }          // reset column
<COMMENTSTATE>.                                 ;                                       // ignore everything in comment

native[ \t]*\{                                  { yyextra->brackets++; yyextra->natblk_lineno = yyget_lineno(yyextra->scanner); yyextra->natblk_column = yyget_column(yyextra->scanner); BEGIN(NATBLKSTATE); }                
<NATBLKSTATE>\{                                 { yyextra->brackets++; add_natblk_string(yyextra,yytext); }                
<NATBLKSTATE>\}                                 { yyextra->brackets--; if (yyextra->brackets == 0) { BEGIN(INITIAL); yylval->token = new_parse_token_without_alloc(yyextra,yyextra->natblk_lineno,yyextra->natblk_column,NATBLOCK,get_natblk(yyextra)); return NAT_BLOCK; } else add_natblk_string(yyextra,yytext); }
<NATBLKSTATE>\"(\\[^\n\r]|[^\n\r"\\])*\"        { add_natblk_string(yyextra,yytext); } 
<NATBLKSTATE>\'(\\[^\n\r]|[^\n\r'\\])*\'        { add_natblk_string(yyextra,yytext); } 
<NATBLKSTATE>\n                                 { add_natblk_string(yyextra,yytext); yyset_column(0,yyscanner); }          
<NATBLKSTATE>.                                  { add_natblk_string(yyextra,yytext); }

\"\"\"                                          { yyextra->natblk_lineno = yyget_lineno(yyextra->scanner); yyextra->natblk_column = yyget_column(yyextra->scanner); add_natblk_string(yyextra,"\""); BEGIN(TXTBLKSTATE); }
<TXTBLKSTATE>\"\"\"                             { BEGIN(INITIAL); add_natblk_string(yyextra,"\""); yylval->token = new_parse_token_without_alloc(yyextra,yyextra->natblk_lineno,yyextra->natblk_column,STRING,get_natblk(yyextra)); return TOKEN; }
<TXTBLKSTATE>\\|\"|\'                           { add_natblk_string(yyextra,"\\"); add_natblk_string(yyextra,yytext); } 
<TXTBLKSTATE>\n                                 { add_natblk_string(yyextra,"\\n"); yyset_column(0,yyscanner); }          
<TXTBLKSTATE>.                                  { add_natblk_string(yyextra,yytext); }

[ \t\r]                                         ;                                       
\n                                              { yyset_column(0,yyscanner); }

public                                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
private                                         { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
transparent                                     { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
opaque                                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
native|extern                                   { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
register|volatile                               { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
include                                         { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
trait                                           { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
type|struct|union|enum|func|as                  { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
test|testgear                                   { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
like|extends                                    { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
augment|with                                    { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
if|unless|else                                  { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
return                                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
assert                                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
or|and                                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
do|while|until|for                              { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
switch|case|default                             { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
break|continue                                  { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
on_init|on_copy|on_move|on_fini|finally         { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),KEY,yytext); return TOKEN; }
signed|unsigned|const|static                    { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),MODIFIER,yytext); return TOKEN; }
inline                                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),MODIFIER,yytext); return TOKEN; }

sizeof|structof                                 { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
:base|:disp                                     { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
goto                                            { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),UNKNOWN,"goto is not allowed. Replacements for the most common use cases are: 1) labelled (outer) loops and 2) finally sections"); return TOKEN; }

null                                            { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),PTR,yytext); return TOKEN; }
true|false                                      { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BOOL,yytext); return TOKEN; }
[0-9]+                                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),INT,yytext); return TOKEN; }
0x[0-9a-fA-F]+                                  { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),INT,yytext); return TOKEN; }
[0-9]*\.[0-9]+([eE][+-]?[0-9]+)?                { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),FLOAT,yytext); return TOKEN; }
\"(\\.|[^\n"\\])*[\n\r]                         { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),UNKNOWN,"illegal newline in string literal"); return TOKEN; }
\"(\\.|[^\n"\\])*\"                             { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),STRING,yytext); return TOKEN; }
\'(\\.|[^\n'\\])                                { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),UNKNOWN,"illegal character literal"); return TOKEN; }
\'(\\.|[^\n'\\])\'                              { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),CHAR,yytext); return TOKEN; }
\'(\\.|[^\n'\\])\'[a-zA-Z_][0-9a-zA-Z_]*        { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),CHAR,yytext); return TOKEN; }

pnc_[a-zA-Z_][0-9a-zA-Z_]*                      { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),UNKNOWN,"illegal identifier starting with pnc_ - reserved for internal use"); return TOKEN; }
PNC_[a-zA-Z_][0-9a-zA-Z_]*                      { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),UNKNOWN,"illegal identifier starting with PNC_ - reserved for internal use"); return TOKEN; }
[a-zA-Z_][0-9a-zA-Z_]*                          { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),IDENT,yytext); return TOKEN; }
\@[a-zA-Z_][0-9a-zA-Z_]*                        { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),ANNOTATION,yytext); return TOKEN; }
\$[a-zA-Z_][0-9a-zA-Z_]*                        { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),ENV_STRING,yytext); return TOKEN; }
\#[a-zA-Z_][0-9a-zA-Z_]*                        { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),ENV_NUMBER,yytext); return TOKEN; }

[&|<>=~^!?:*,./%+-]                             { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
\+\+|\-\-|\<\<|\>\>|\&\&|\|\|                   { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
==|!=|\<=|\>=|::                                { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
\+=|\-=|\*=|\/=|\%=|\<\<=|\>\>=|\&=|\^=|\|=     { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }
\.{3}                                           { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return TOKEN; }

\#\[                                            { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; }
\#provide\[                                     { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; }
\#define\[                                      { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; }
\(|\{|\[                                        { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; }
struct[ \n\t\r]*\{                              { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous struct
union[ \n\t\r]*\{                               { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous union
enum[ \n\t\r]*\{                                { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // anonymous enum
with[ \n\t\r]*\{                                { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_OPEN; } // with block
\)|\}|\]                                        { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),BLOCK,yytext); return BLOCK_CLOSE; }

\;                                              { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),OP,yytext); return SEMICOLON; }

.                                               { yylval->token = new_parse_token(yyextra,yyget_lineno(yyextra->scanner),yyget_column(yyextra->scanner),UNKNOWN,"illegal character"); return TOKEN; }

%%

