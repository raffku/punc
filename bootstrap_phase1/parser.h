#ifndef PARSER_H
#define PARSER_H

enum TokenKind; /* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:85,8 - TokenKind */
struct ParseContext; /* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:166,8 - ParseContext */
struct ParseToken; /* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:196,8 - ParseToken */


/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:85,8 - TokenKind */
enum TokenKind {
    UNKNOWN,
    ENV_NUMBER,
    ENV_STRING,
    ANNOTATION,
    KEY,
    MODIFIER,
    IDENT,
    OP,
    BOOL,
    PTR,
    INT,
    FLOAT,
    STRING,
    CHAR,
    BLOCK,
    NATBLOCK
};

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:166,8 - ParseContext */
struct ParseContext {
    char* id;
    void* scanner;
    void* statements;
    int brackets;
    char* natblk;
    int natblk_size;
    int natblk_used;
    unsigned int natblk_lineno;
    unsigned int natblk_column;
};

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:196,8 - ParseToken */
struct ParseToken {
    struct ParseContext* ctx;
    unsigned int lineno;
    unsigned int column;
    enum TokenKind kind;
    char* text;
};

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:210,8 - new_parse_token */
extern struct ParseToken* new_parse_token(struct ParseContext* ctx, unsigned int lineno, unsigned int column, enum TokenKind kind, char* text);

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:221,8 - new_parse_token_without_alloc */
extern struct ParseToken* new_parse_token_without_alloc(struct ParseContext* ctx, unsigned int lineno, unsigned int column, enum TokenKind kind, char* text);

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:232,8 - free_parse_token */
extern void free_parse_token(struct ParseToken* tok);

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:237,8 - insert_token */
extern void insert_token(struct ParseToken* tok);

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:245,8 - close_block */
extern void close_block(struct ParseToken* tok);

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:280,8 - close_statement */
extern void close_statement(struct ParseToken* tok);

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:291,8 - add_natblk_string */
extern void add_natblk_string(struct ParseContext* ctx, char* s);

/* /home/kuno/Dokumente/Devel/punc/src_old/parser.ac:296,8 - get_natblk */
extern char* get_natblk(struct ParseContext* ctx);

#endif /*PARSER_H*/