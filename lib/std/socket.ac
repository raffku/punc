/* Copyright (c) 2020 Raffin Kuno

  This file is in the pulic domain.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// nice for debugging
//public native include "stdio.h" {
//   int printf(const char *format, ...);
//}

public test "Socket cheatsheet" {
   assert Address addr.tcp("localhost","25") == 0;    // retrieve one or more socket infosets to try to connect to, see also Address.udp()
   //Address addr2 = addr;                            // compiler error - Address objects cannot be assigned
   //addr.tcp("localhost","25");                      // assertion - you need to free() addr before reusing it
   auto io = addr.connect();                          // connect to one of these infosets and retrieve a Socket instance
   assert io.valid();
   Socket io2 = io;                                   // Socket objects can be assigned, but io will no longer be valid
   assert io2.valid();
   assert !io.valid();
   //io2 = addr.connect();                            // assertion - valid Socket objects cannot be initialized again
   assert io2.blocking();
   assert io2.set_blocking(false) == 0;               // make io nonblocking
   assert !io2.blocking();
   assert addr.free();                                // you can free addr explicitly after connecting, and then reuse it for a new request. Would happen automatically when sock goes out of scope
   assert io2.close() == 0;                           // you can close io explicitly, but that would also happen automatically when it goes out of scope
   
   assert addr.tcp("localhost","25") == 0;            // aquire the connection info again - had it freed just to make a point
   auto io3 = addr.connect_nb();                      // connect_nb() returns a non-blocking socket. The connect itself may not yet have finished and could still fail
   assert io3.valid();
   assert !io3.blocking();
   
   assert io3.set_blocking(true) == 0;                // make io3 blocking for a small read test
   char buf[128];
   assert io3.read(buf,sizeof(buf)) > 0;              // SMTP server will greet me with a nice welcome message - see also Socket.write(), but we will not implement an SMTP client here...
   
   assert Address saddr.tcp("localhost","8888") == 0;
   assert auto sio = saddr.listen_nb(5);              // create a listening socket on the address - listen_nb returns a non-blocking socket, so subsequent accepts will not block
   auto cio = sio.accept_nb();                        // no connections will be pending, but sio is non-blocking, so accept and accept_nb return immediately ...
   assert !cio.valid();                               // ... and the accepted socket is invalid
}

//////////////////////

public type Address like addrinfo* = null {
   inline on_fini(self): if (*self:base) freeaddrinfo(*self:base);
}

public augment Address {
   int tcp(Address* self != null, const char* node != null, const char* service != null) {
      assert *self == null;                                 // require an empty result pointer
      addrinfo hints;
      hints.ai_family = AF_UNSPEC;                          // allow IPv4 or IPv6
      hints.ai_socktype = SOCK_STREAM; 
      hints.ai_flags = 0;
      hints.ai_protocol = 0;                                // allow any protocol
      return getaddrinfo(node,service,&hints,self:base);
   }
   
   int udp(Address* self != null, const char* node != null, const char* service != null) {
      assert *self == null;                                 // require an empty result pointer
      addrinfo hints;
      hints.ai_family = AF_UNSPEC;                          // allow IPv4 or IPv6
      hints.ai_socktype = SOCK_DGRAM; 
      hints.ai_flags = 0;
      hints.ai_protocol = 0;                                // allow any protocol
      return getaddrinfo(node,service,&hints,self:base);
   }
   
   // TODO maybe create unix_stream(Address* self != null, const char* path != null) and unix_dgram(Address* self != null, const char* path != null)
   // for those create addrinfo by hand, see https://github.com/ryuever/unix-network-programming-v3/blob/master/libfree/getaddrinfo.c
   
   // create a blocking socket connected to the address
   Socket connect(const Address self != null) {
      // try addresses in order
      for (const addrinfo* rp = self:base; rp != null; rp = rp.ai_next) {
         int fd = socket(rp.ai_family,rp.ai_socktype|SOCK_CLOEXEC,rp.ai_protocol);  // enable close-on-exec by default
         if (fd == -1) continue;                                                    // could not create socket
         if (connect(fd,rp.ai_addr,rp.ai_addrlen) != -1) native { return fd; }      // if connect succeeds, return file descriptor
         close(fd);                                                                 // socket created, but connect failed, close the socket
      }
      // failed
      native { return -1; }
   }
   
   // create a non-blocking socket connected to the address
   Socket connect_nb(const Address self != null) {
      // try addresses in order
      for (const addrinfo* rp = self:base; rp != null; rp = rp.ai_next) {
         int fd = socket(rp.ai_family,rp.ai_socktype|SOCK_CLOEXEC|SOCK_NONBLOCK,rp.ai_protocol);         // enable close-on-exec by default, also set the non-blocking flag
         if (fd == -1) continue;                                                                         // could not create socket
         if (connect(fd,rp.ai_addr,rp.ai_addrlen) != -1 || errno == EINPROGRESS) native { return fd; }   // if connect succeeds or only fails because it would block, return file descriptor
         close(fd);                                                                                      // socket created, but connect failed, close the socket
      }
      // failed
      native { return -1; }
   }
   
   // create a blocking socket listening on the address
   Socket listen(const Address self != null, int backlog) {
      int fd = -1;
      // try addresses in order
      for (const addrinfo* rp = self:base; rp != null; rp = rp.ai_next) {
         fd = socket(rp.ai_family,rp.ai_socktype|SOCK_CLOEXEC,rp.ai_protocol);      // enable close-on-exec by default
         if (fd == -1) continue;                                                    // could not create socket
         int on = 1; setsockopt(fd,SOL_SOCKET, SO_REUSEADDR,&on,sizeof(on));
         if (bind(fd,rp.ai_addr,rp.ai_addrlen) != -1) break;                        // if connect succeeds, use this
         close(fd); fd = -1;                                                        // socket created, but connect failed, close the socket
      }
      // put socket into the listening state
      if (fd != -1) {
         if (listen(fd,backlog) == -1) {
            close(fd); fd = -1;
         }
      }
      // return result
      native { return fd; }
   }
   
   // create a non-blocking socket listening on the address - this means, subsequent accept() calls will not block
   Socket listen_nb(const Address self != null, int backlog) {
      Socket io = self.listen(backlog);
      if (io != -1) {
         io.set_blocking(false) == 0 or io.close();
      }
      native { return io; }
   }
   
   Address* free(Address* self != null) {
      assert *self != null;
      freeaddrinfo(*self:base);
      *self:base = null;
      return self;
   }
}

///////////////////////

public type Socket like int = -1 {
   inline on_move(self,other):   assert *self == -1; *self:base = *other:base; *other:base = -1;
   inline on_fini(self):         if (*self != -1) close(*self:base);
}

public augment Socket {
   inline int fd(const Socket self) {
      return self:base;
   }
   
   inline bool valid(const Socket self) {
      return self != -1;
   }
   
   inline Self* set(Self* self, Self* other) {
      *self = *other;
      return self;
   }

   inline bool blocking(const Socket self != -1) {
      return !(fcntl(self,F_GETFL) & O_NONBLOCK);
   }
   
   int set_blocking(const Socket self != -1, bool block) {
      int flags = fcntl(self,F_GETFL);
      if (block) flags &= (~O_NONBLOCK);
      else flags |= O_NONBLOCK;
      return fcntl(self,F_SETFL,flags);
   }
   
   inline Socket accept(const Socket self != -1) native {
      return accept4(self,0,0,SOCK_CLOEXEC); // enable close-on-exec on accepted socket by default
   }

   inline Socket accept_nb(const Socket self != -1) native {
      return accept4(self,0,0,SOCK_CLOEXEC|SOCK_NONBLOCK); // enable close-on-exec on accepted socket by default, accepted socket will be non-blocking
   }

   int close(Socket* self != null) {
      assert *self != -1;
      return close(*self:base);
   finally:
      *self = -1;
   }
}

public [T] augment Socket {
   inline ssize_t read(const Socket self != -1, T* objects != null, uint object_count) {
      return recv(self,objects,sizeof(T)*object_count,0);
   }

   inline ssize_t write(const Socket self != -1, const T* objects != null, uint object_count) {
      return send(self,objects,sizeof(T)*object_count,0);
   }
}

///////////////////////

native include "sys/types.h", "sys/socket.h", "netdb.h" with "_GNU_SOURCE" {
   type size_t like uint;
   type ssize_t like int;
   type socklen_t like uint;
   struct sockaddr {
   }
   struct addrinfo {
      int              ai_flags;
      int              ai_family;
      int              ai_socktype;
      int              ai_protocol;
      socklen_t        ai_addrlen;
      sockaddr        *ai_addr;
      char            *ai_canonname;
      addrinfo        *ai_next;
   }
   
   const int AF_UNSPEC;
   const int AF_INET;
   const int AF_INET6;
   const int AF_UNIX;
   const int SOCK_STREAM;
   const int SOCK_DGRAM;
   const int SOCK_CLOEXEC;
   const int SOCK_NONBLOCK;
   
   const int SOL_SOCKET;
   const int SO_REUSEADDR;
   
   int getaddrinfo(const char *node, const char *service, const addrinfo* hints, addrinfo** res);
   void freeaddrinfo(addrinfo* res);
   const char* gai_strerror(int errcode);
   
   int socket(int domain, int typ, int protocol);
   int connect(int sockfd, const sockaddr *addr, socklen_t addrlen);
   int bind(int sockfd, const sockaddr *addr, socklen_t addrlen);
   int listen(int sockfd, int backlog);
   int accept4(int sockfd, sockaddr *addr, socklen_t *addrlen, int flags);
   
   int getsockopt(int sockfd, int level, int optname, void *optval, socklen_t *optlen);
   int setsockopt(int sockfd, int level, int optname, const void *optval, socklen_t optlen);
}

native include "unistd.h", "fcntl.h" {
   const int F_GETFD;
   const int F_SETFD;
   const int F_GETFL;
   const int F_SETFL;
   
   const int O_CLOEXEC;
   const int O_NONBLOCK;

   int fcntl(int fd, int cmd, ...);
   int close(int fd);
   
   ssize_t recv(int sockfd, void *buf, size_t len, int flags);
   ssize_t send(int sockfd, const void *buf, size_t len, int flags);
}

native include "errno.h" {
   int errno;
   const int EINPROGRESS;
}
