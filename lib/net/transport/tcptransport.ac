/* Copyright (c) 2020 Raffin Kuno

  This file is in the pulic domain.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

include "std/socket";
include "std/string";
include "net/iorunner";
include "net/infra/buffer";
include "iotransportrider";

public [R:IOTransportRider] type TCPTransport {
    Socket sock;
    IOTransportData data;
    R rider;
}

public [R:IOTransportRider] augment TCPTransport[R] {
    Self* connect_to(Self* self, const char* server, const char* port) {
        Address addr.tcp(server,port) and return null;
        self.sock = addr.connect_nb();
        return self;
    }

    Self* use(Self* self, Socket sock.valid()) {
        assert !sock.blocking();
        self.sock = sock;
        return self;
    }
      
    inline R* mod_rider(Self* self != null) {
        return &self.rider;
    }
}

private [R:IOTransportRider] augment TCPTransport[R] with IOStream {
    Socket up {
        self.rider.up((IOTransportControl*)&self.data,(IOStreamControl*)ctrl);
        self.register_events(ctrl);
        if (self.data.get_idle_timeout() > 0) {
            ctrl.set_timeout(self.data.get_idle_timeout());
        }
        return self.sock;
    }
        
    void io {
        self.io_in(sock,events,ctrl) or { ctrl.stop(); return; }
        self.io_out(sock,events,ctrl) or { ctrl.stop(); return; }
        if (self.data.is_shutting_down()) {
            ctrl.stop();
        } else {
            self.register_events(ctrl);
            if (self.data.get_idle_timeout() > 0) {
                ctrl.set_timeout(self.data.get_idle_timeout());
            }
        }
    }
    
    bool io_in(Self* self != null, const Socket sock, uint events, IOControl* ctrl != null) {
        if (events & IO_IN) {
            // read
            int rlen = sock.read(self.data.get_inbuf().wptr(),self.data.get_inbuf().cap());
            if (rlen > 0) {
                self.data.get_inbuf().commit(rlen);
            } else {
                return false;
            }
            // parse when new input is available
            self.rider.parse((IOTransportControl*)&self.data,(IOStreamControl*)ctrl);
        }
        // success
        return true;
    }
        
    bool io_out(Self* self != null, const Socket sock, uint events, IOControl* ctrl != null) {
        if (events & IO_OUT) {
            // write
            int wlen = sock.write(self.data.get_outbuf().rptr(),self.data.get_outbuf().avl());
            if (wlen > 0) {
                self.data.get_outbuf().evict(wlen);
            } else {
                return false;
            }
        }
        // replenish output buffer whenever capacity is available
        if (self.data.get_outbuf().sweep().cap() > 0) {
            self.rider.write((IOTransportControl*)&self.data,(IOStreamControl*)ctrl);
        }
        // success
        return true;
    }
    
    void register_events(Self* self != null, IOControl* ctrl != null) {
        int events = 0;
        if (self.data.get_inbuf().sweep().cap() > 0) events = events | IO_IN;
        if (self.data.get_outbuf().avl() > 0) events = events | IO_OUT;
        ctrl.set_events(events);
    }
        
    inline void time { assert; }
    inline void down { self.rider.down(sock,(IOTransportControl*)&self.data,(IOStreamControl*)ctrl); }
}

