/* Copyright (c) 2020 Raffin Kuno

  This file is in the pulic domain.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

public testgear {
    include "net/iorunner";
    include "std/socket";

    augment int with IOConnectionFactory {
        void connect {
            ctrl.new[TestStream]();
        }
    }

    type TestStream { int dummy; }
    augment TestStream with IOStream {
        auto up { ctrl.stop(); return Socket invalid; }
        auto io {}
        auto time {}
        auto down {}
    }
}

public test "IOListener" {
    IORunner runner;
    runner.new[IOListener[int]]().listen_on("localhost","8080");
    //runner.run();
}

//////////////////////////////////////////////////

include "std/string";
include "std/socket";
include "net/iorunner";
public include "iostreamcontrol";

public trait IOConnectionFactory {
    void connect(Self* self != null, IOStreamControl* ctrl != null, Socket sock);
}

public [F:IOConnectionFactory] type IOListener {
    Address addr;
    uint backlog = 10;
    F factory;
}

public [F:IOConnectionFactory] augment IOListener[F] {
    Self* set_backlog(Self* self != null, uint backlog > 0) {
        self.backlog = backlog;
        return self;
    }

    Self* listen_on(Self* self != null, const char* addr, const char* port) {
        self.addr.tcp(addr,port) and return null;
        return self;
    }

    F* mod_factory(Self* self != null) {
        return &self.factory;
    }
}

private [F:IOConnectionFactory] augment IOListener[F] with IOStream {
    Socket up {
        Socket sock = self.addr.listen_nb(self.backlog);
        ctrl.set_events(IO_IN);
        assert sock.valid();
        return sock;
    }

    void io {
        Socket clientsock = sock.accept_nb();
        if (clientsock.valid()) self.factory.connect((IOStreamControl*)ctrl,clientsock);
    }

    inline void time { assert; }
    inline void down { }
}

