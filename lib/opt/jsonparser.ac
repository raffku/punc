/* Copyright (c) 2023 Raffin Kuno

  This file is in the pulic domain.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

public testgear {
    include "json";
    include "reader";
    include "std/string";
    include "coll/queue";
    include "coll/map";
    include "coll/strings";
    include "std/logger"; Logger log.reg();
}

public test "JsonParsedValue" {
    JsonParsedValue v1;
    assert Reader::progressive_read(v1,"\"abc\\u0064\"") == read_ok;
    assert v1.is_string();
    assert v1.as_string().eqs("abcd");

    JsonParsedValue v2;
    assert Reader::progressive_read(v2,"[\"abc\",12,null,false,{\"a\":1,\"b\":2}]") == read_ok;
    assert v2.is_array();
    assert v2.array().size() == 5;
    assert v2.to_string().eqs("[\"abc\",12,null,false,{\"a\":1,\"b\":2}]");

    auto o1 = v2.array().tail();
    assert o1.is_object();
    assert o1.object().size() == 2;
    assert o1.object().gets("a").as_int() == 1;
}

/////////////////////////////////

include "json";
include "reader";
include "writer";
include "std/base";
include "std/string";
include "coll/queue";
include "coll/strings";
include "coll/map";
include "std/shared";

public trait JsonParsedValue extends JsonValue+Readable {
    on_move: default;
}

public augment JsonParsedValue with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        if (self.is[void]()) {
            if (reader.skipws().avl() == 0) return read_need_more;
            switch (reader.rptr()[0]) {
                case '"': return self.new[JsonParsedString]().read(reader);
                case '[': return self.new[JsonParsedArray]().read(reader);
                case '{': return self.new[JsonParsedObject]().read(reader);
                case 'n': return self.new[JsonParsedNull]().read(reader);
                case 't': return self.new[JsonParsedTrue]().read(reader);
                case 'f': return self.new[JsonParsedFalse]().read(reader);
                default:  return self.new[JsonParsedNumber]().read(reader);
            }
            return read_syntax_error;
        } else {
            return self.read:disp(reader);
        }
    }

    void to_json(const Self self, Writer* writer != null) {
        if (self.is[void]()) {
            writer.put("null",4);
        } else {
            self.to_json:disp(writer);
        }
    }
}

public augment JsonParsedValue {
    inline bool is_string(const Self self) { return self.is[JsonParsedString](); }
    inline bool is_double(const Self self) { return self.is[JsonParsedNumber](); }
    inline bool is_llong(const Self self) { return self.is[JsonParsedNumber]() && self.get[JsonParsedNumber]().is_llong(); }
    inline bool is_long(const Self self) { return self.is[JsonParsedNumber]() && self.get[JsonParsedNumber]().is_long(); }
    inline bool is_int(const Self self) { return self.is[JsonParsedNumber]() && self.get[JsonParsedNumber]().is_int(); }
    inline bool is_short(const Self self) { return self.is[JsonParsedNumber]() && self.get[JsonParsedNumber]().is_short(); }
    inline bool is_bool(const Self self) { return self.is[JsonParsedTrue]() || self.is[JsonParsedFalse](); }
    inline bool is_null(const Self self) { return self.is[JsonParsedNull](); }

    inline bool is_array(const Self self) { return self.is[JsonParsedArray](); }
    inline bool is_object(const Self self) { return self.is[JsonParsedObject](); }

    String as_string(const Self self.is_string()) {
        return self.get[JsonParsedString]().as_string();
    }

    double as_double(const Self self.is_double()) {
        return self.get[JsonParsedNumber]().as_double();
    }

    llong as_llong(const Self self.is_llong()) {
        return self.get[JsonParsedNumber]().as_llong();
    }

    long as_long(const Self self.is_long()) {
        return self.get[JsonParsedNumber]().as_long();
    }

    int as_int(const Self self.is_int()) {
        return self.get[JsonParsedNumber]().as_int();
    }

    short as_short(const Self self.is_short()) {
        return self.get[JsonParsedNumber]().as_short();
    }

    bool as_bool(const Self self.is_bool()) {
        assert self.is_bool();
        return self.is[JsonParsedTrue]();
    }

    const Queue[JsonParsedValue]* array(const Self* self != null) {
        auto arr = self.get[JsonParsedArray]() or assert;
        return arr.array();
    }

    const Map[String,String::cmp,JsonParsedValue]* object(const Self* self != null) {
        auto arr = self.get[JsonParsedObject]() or assert;
        return arr.object();
    }
}

/////////////////////////////////

private type JsonParsedString {
    Queue[String] parts;
    bool esc = false;
on_move:
    self.parts.swap(other.parts);
    self.esc = other.esc;
}

private inline const uint partsize = 512;
private augment JsonParsedString with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        Reader subreader.from_reader(reader);
        // consume start quote
        if (self.parts.empty()) {
            subreader.skipws();
            subreader.reqc('"') and return _;
        }
        // create parts
        while (subreader.avl()) {
            // prepare part and writer
            auto part = self.parts.new() or return read_insufficient_memory;
            auto wptr = part.new(partsize) or return read_insufficient_memory;
            Writer writer.to_buf(wptr,partsize);
            // read part
            auto res = self.read_one_part(subreader,writer);
            if (res == read_ok) {
                // concat parts
                if (self.parts.size() > 1) {
                    String joined = self.as_string();
                    self.parts.clr().new().cpy(joined);
                }
                // done
                reader.consume(subreader.len());
                return read_ok;
            } else if (res == read_need_more && writer.overflow()) {
                // continue with next part
            } else if (res == read_need_more) {
                // insufficient input
                break;
            } else {
                // error
                reader.consume(subreader.len());
                return res;
            }
        }
        // check if we can consume
        unless (self.parts.empty()) {
            reader.consume(subreader.len());
        }
        // more input to follow
        return read_need_more;
    }

    ReadResult read_one_part(Self* self != null, Reader* reader != null, Writer* writer != null) {
        writer.wptr()[0] = '\0';
        // read characters
        while (reader.avl()) {
            // end-of-string?
            switch (reader.peekc()) {
                case '\0': return read_need_more;
                case '"':  reader.consume(1); return read_ok;
            }
            // read character - consumes 1,2 or 6 bytes, writes 1,2 or 3 bytes
            auto res = self.read_one_character(reader,writer);
            writer.wptr()[0] = '\0';
            if (res != read_ok) return res;
        }
        // more parts to follow
        return read_need_more;
    }

    ReadResult read_one_character(Self* self != null, Reader* reader != null, Writer* writer != null) {
        Reader subreader.from_reader(reader);
        Writer subwriter.to_writer(writer);
        switch (char char0 = subreader.getc()) {
            case '\0':
                return read_need_more;
            case '\\':
                switch (char char1 = subreader.getc()) {
                    case '\0': return read_need_more;
                    case 'u':
                        if (subreader.avl() < 4) return read_need_more;
                        // get unicode codepoint
                        int a = hex(subreader.getc());
                        int b = hex(subreader.getc());
                        int c = hex(subreader.getc());
                        int d = hex(subreader.getc());
                        if (a<0 || b<0 || c<0 || d<0) return read_syntax_error;
                        // write utf8 character
                        subwriter.put_utf8((uint)0 + (a<<12) + (b<<8) + (c<<4) + (d<<0));
                        break;
                    case 't': subwriter.putc('\t'); break;
                    case 'r': subwriter.putc('\r'); break;
                    case 'n': subwriter.putc('\n'); break;
                    case 'b': subwriter.putc('\b'); break;
                    case 'f': subwriter.putc('\f'); break;
                    default:  subwriter.putc(char1); break;
                }
                break;
            default:
                subwriter.putc(char0);
                break;
        }
        // undo reads if we could not write
        if (subwriter.overflow()) {
            writer.commit(subwriter.len());
            return read_need_more;
        }
        // success, consume and commit bytes
        reader.consume(subreader.len());
        writer.commit(subwriter.len());
        return read_ok;
    }

    String as_string(const Self* self != null) {
        if (self.parts.size() == 1) {
            return *self.parts.head();
        } else {
            return String::join(self.parts,"");
        }
    }

    inline void to_json(const Self self, Writer* writer != null) {
        self.as_string().to_json(writer);
    }
}

private int hex(const char c) {
     if (c >= '0' && c <= '9') {
        return c - '0';
    } else if (c >= 'a' && c <= 'f') {
        return c + 10 - 'a';
    } else if (c >= 'A' && c <= 'F') {
        return c + 10 - 'A';
    } else {
        return -1;
    }
}

private void Writer::put_utf8(Writer* writer != null, const uint codepoint <= 0xFFFF) {
    if (codepoint <= 0x7F) {
        writer.putc((const char) codepoint);
    } else if (codepoint <= 0x07FF) {
        writer.putc((const char) (((codepoint >> 6) & 0x1F) | 0xC0));
        writer.putc((const char) (((codepoint >> 0) & 0x3F) | 0x80));
    } else {
        writer.putc((const char) (((codepoint >> 12) & 0x0F) | 0xE0));
        writer.putc((const char) (((codepoint >>  6) & 0x3F) | 0x80));
        writer.putc((const char) (((codepoint >>  0) & 0x3F) | 0x80));
    }
}

private test "JsonParsedString" {
    //Logger::set_threshold_for("progressive_read",trace);

    JsonParsedString s1;
    assert Reader::progressive_read(s1,"abc") == read_syntax_error;

    JsonParsedString s2;
    assert Reader::progressive_read(s2," ") == read_need_more;

    JsonParsedString s3;
    assert Reader::progressive_read(s3,"\"\\uffff\"") == read_ok;
    assert s3.as_string().len() == 3;

    // check that chunking works
    String longstring;
    auto ptr = longstring.new(partsize*6+2);
    Writer writer.to_buf(ptr,partsize*6+2+1);
    writer.putc('"');
    for (int i=0; i<partsize; i++) {
        writer.put("\\uffff",6);
    }
    writer.putc('"');
    writer.putc('\0');
    // parse
    Reader r4.from_str(longstring.str());
    assert JsonParsedString s4.read(r4) == read_ok;
    assert s4.as_string().len() == partsize*3;
}

/////////////////////////////////

private type JsonParsedNumber {
    bool is_integer = true;
    double dnumber;
    llong  inumber;
on_copy:
    default;
}

private augment JsonParsedNumber with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        static char buf[256];
        // find length of number
        int len = reader.skipws().indexofnone("+-1234567890.eE",15);
        if (len < 0) len = reader.avl();
        if (len >= sizeof(buf)) return read_syntax_error;
        // copy number into buffer
        memcpy(buf,reader.rptr(),len);
        buf[len] = '\0';
        // parse as double
        {
            errno = 0;
            char* endptr;
            self.dnumber = strtod(buf,&endptr);
            if (errno != 0 || *endptr != '\0') return read_syntax_error;
        }
        // parse as llong
        {
            errno = 0;
            char* endptr;
            self.inumber = strtoll(buf,&endptr,10);
            self.is_integer = (errno == 0 && *endptr == '\0'); // see if this worked without errors
        }
        // if we are sure to have the complete number, consume bytes
        if (len < reader.avl()) {
            reader.consume(len);
            return read_ok;
        } else {
            return read_need_more;
        }
    }

    inline double as_double(const Self* self != null)   { return self.dnumber; }

    inline bool is_llong(const Self self)               { return self.is_integer; }
    inline llong as_llong(const Self* self != null)     { assert self.is_llong(); return self.inumber; }

    inline bool is_long(const Self self)                { return self.is_integer && self.inumber <= LONG_MAX; }
    inline long as_long(const Self* self != null)       { assert self.is_long(); return (const long)self.inumber; }

    inline bool is_int(const Self self)                 { return self.is_integer && self.inumber <= INT_MAX; }
    inline int as_int(const Self* self != null)         { assert self.is_int(); return (const int)self.inumber; }

    inline bool is_short(const Self self)               { return self.is_integer && self.inumber <= SHRT_MAX; }
    inline short as_short(const Self* self != null)     { assert self.is_short(); return (const short)self.inumber; }

    void to_json(const Self self, Writer* writer != null) {
        if (self.is_llong()) {
            self.as_llong().to_json(writer);
        } else {
            self.as_double().to_json(writer);
        }
    }
}

private test "JsonParsedNumber" {
    Reader r1.from_str("123");
    assert JsonParsedNumber n1.read(r1) == read_need_more;
    assert r1.len() == 0; // if number is not delimited, it is not yet consumed
    Reader r1a.from_str("123 ");
    assert n1.read(r1a) == read_ok;
    assert r1a.len() == 3;
    assert n1.is_int() && n1.as_int() == 123;

    JsonParsedNumber n2;
    assert Reader::progressive_read(n2,"1.2,") == read_ok;
    assert n2.as_double() == 1.2;
}

/////////////////////////////////

private type JsonParsedNull {
    on_copy: default;
}

private augment JsonParsedNull with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        reader.skipws().avl() >= 4 or return read_need_more;
        // read null
        if (memcmp(reader.rptr(),"null",4) == 0) {
            reader.consume(4);
            return read_ok;
        } else {
            return read_syntax_error;
        }
    }

    void to_json(const Self self, Writer* writer != null) {
        writer.put("null",4);
    }
}

/////////////////////////////////

private type JsonParsedTrue {
    on_copy: default;
}

private augment JsonParsedTrue with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        reader.skipws().avl() >= 4 or return read_need_more;
        // read null
        if (memcmp(reader.rptr(),"true",4) == 0) {
            reader.consume(4);
            return read_ok;
        } else {
            return read_syntax_error;
        }
    }

    void to_json(const Self self, Writer* writer != null) {
        writer.put("true",4);
    }
}

/////////////////////////////////

private type JsonParsedFalse {
    on_copy: default;
}

private augment JsonParsedFalse with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        reader.skipws().avl() >= 5 or return read_need_more;
        // read null
        if (memcmp(reader.rptr(),"false",5) == 0) {
            reader.consume(5);
            return read_ok;
        } else {
            return read_syntax_error;
        }
    }

    void to_json(const Self self, Writer* writer != null) {
        writer.put("false",5);
    }
}

/////////////////////////////////

private type JsonParsedArray {
    Queue[JsonParsedValue] arr;
    JsonParsedValue* current_value = null;
on_move:
    self.arr.swap(other.arr);
    self.current_value = other.current_value;
    other.current_value = null;
}

private augment JsonParsedArray with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        while (reader.skipws().avl() > 0) {
            if (self.current_value) {
                // parse current entry
                self.current_value.read(reader) and return _;
                self.current_value = null;
            } else {
                Reader subreader.from_reader(reader);
                // consume opening bracket
                if (self.arr.empty()) {
                    subreader.reqc('[') and return _;
                    subreader.skipws().avl() or return read_need_more;
                }
                // check for closing bracket or prepare next value
                if (subreader.peekc() == ']') {
                    subreader.consume(1);
                    reader.consume(subreader.len());
                    return read_ok;
                } else if (subreader.peekc() == ',') {
                    if (self.arr.empty()) return read_syntax_error; // we can not get here for the first entry
                    subreader.consume(1);
                    self.current_value = self.arr.new() or return read_insufficient_memory;
                    reader.consume(subreader.len());
                } else {
                    unless (self.arr.empty()) return read_syntax_error; // we can get here only for the first entry
                    self.current_value = self.arr.new() or return read_insufficient_memory;
                    reader.consume(subreader.len());
                }
            }
        }
        // will read more entries when called again
        return read_need_more;
    }

    inline const Queue[JsonParsedValue]* array(const Self* self != null) {
        return &self.arr;
    }

    void to_json(const Self self, Writer* writer != null) {
        self.array().to_json(writer);
    }
}

private test "JsonParsedArray" {
    JsonParsedArray a1;
    assert Reader::progressive_read(a1,"[ ]") == read_ok;
    assert a1.array().size() == 0;

    JsonParsedArray a2;
    assert Reader::progressive_read(a2,"[ , ]") == read_syntax_error;
}

/////////////////////////////////

public type JsonParsedObject {
    Map[String,String::cmp,JsonParsedValue] map;
    Shared[JsonParsedPair] current_pair;
on_move:
    self.map.swap(other.map);
    self.current_pair = other.current_pair;
    other.current_pair.clr();
}

public augment JsonParsedObject with JsonParsedValue {
    ReadResult read(Self* self != null, Reader* reader != null) {
        while (reader.skipws().avl() > 0) {
            if (auto pair = self.current_pair.mod()) {
                // parse current entry
                pair.read(reader) and return _;
                self.store_current_pair() and return _;
            } else {
                Reader subreader.from_reader(reader);
                // consume opening bracket
                if (self.map.empty()) {
                    subreader.reqc('{') and return _;
                    subreader.skipws().avl() or return read_need_more;
                }
                // check for closing bracket or prepare next value
                if (subreader.peekc() == '}') {
                    subreader.consume(1);
                    reader.consume(subreader.len());
                    return read_ok;
                } else if (subreader.peekc() == ',') {
                    if (self.map.empty()) return read_syntax_error; // we can not get here for the first entry
                    subreader.consume(1);
                    self.current_pair.new() or return read_insufficient_memory;
                    reader.consume(subreader.len());
                } else {
                    unless (self.map.empty()) return read_syntax_error; // we can get here only for the first entry
                    self.current_pair.new() or return read_insufficient_memory;
                    reader.consume(subreader.len());
                }
            }
        }
        // will read more entries when called again
        return read_need_more;
    }

    inline const Map[String,String::cmp,JsonParsedValue]* object(const Self* self != null) {
        return &self.map;
    }

    void to_json(const Self self, Writer* writer != null) {
        self.object().to_json(writer);
    }
}

private ReadResult JsonParsedObject::store_current_pair(Self* self != null) {
    auto pair = self.current_pair.mod() or assert;
    auto entry = self.map.new() or return read_insufficient_memory;
    entry.k = pair.key.as_string();
    entry.v = pair.val;
    self.current_pair.clr();
    return self.map.ins() ? read_syntax_error : read_ok;
}

private test "JsonParsedObject" {
    JsonParsedObject o1;
    assert Reader::progressive_read(o1,"{ }") == read_ok;
    assert o1.object().size() == 0;

    JsonParsedObject o2;
    assert Reader::progressive_read(o2,"{ , }") == read_syntax_error;
}

/////////////////////////////////

public type JsonParsedPair {
    JsonParsedString key;
    JsonParsedValue val;
    ReadResult key_read_result = read_need_more;
    ReadResult col_read_result = read_need_more;
    ReadResult val_read_result = read_need_more;
on_move:
    default;
}

public augment JsonParsedPair {
    ReadResult read(Self* self != null, Reader* reader != null) {
        if (self.key_read_result == read_need_more) {
            self.key_read_result = self.key.read(reader) and return _;
        }
        if (self.col_read_result == read_need_more) {
            self.col_read_result = reader.skipws().reqc(':') and return _;
        }
        if (self.val_read_result == read_need_more) {
            self.val_read_result = self.val.read(reader) and return _;
        }
        return read_ok;
    }
}

/////////////////////////////////

Self* Reader::skipws(Self* self != null) {
    int idx = self.indexofnone(" \t\r\n",4);
    self.consume((idx < 0) ? self.avl() : idx);
    return self;
}

include "std/logger"; Logger log.reg();

native include "string.h" {
    type size_t like uint;
    void *memcpy(void *dest, const void *src, size_t n);
    int memcmp(const void *s1, const void *s2, size_t n);
}

native include "stdlib.h", "errno.h" {
    int errno;
    double strtod(const char *nptr, char **endptr);
    llong strtoll(const char * nptr, char ** endptr, int base);
}
