/* Copyright (c) 2023 Raffin Kuno

  This file is in the pulic domain.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

public testgear {
    include "writer";
    include "std/base";
    include "std/string";
    include "coll/queue";
    include "coll/map";
    include "std/logger"; Logger log.reg();

    native include "string.h" {
        type size_t like uint;
        int memcmp(const void *s1, const void *s2, size_t n);
    }
}

public test "JsonValue" {
    char out[100];

    JsonValue v1;
    Writer w1.to_buf(out,sizeof(out)).put_json(v1);
    assert w1.len() == 4;
    assert memcmp(out,"null",4) == 0;

    JsonValue v2.new[String]().set("xxx\t\r\n\b\f");
    Writer w2.to_buf(out,sizeof(out)).put_json(v2);
    assert w2.len() == 15;
    assert memcmp(out,"\"xxx\\t\\r\\n\\b\\f\"",15) == 0;

    Writer w3.to_buf(out,sizeof(out)).put_json(true);
    assert w3.len() == 4;
    assert memcmp(out,"true",4) == 0;

    Writer w4.to_buf(out,2).put_json((const byte)15);
    assert w4.len() == 2;
    assert memcmp(out,"15",2) == 0;

    Queue[String] a1;
    a1.new().set("abc");
    a1.new().set("xyz");
    Writer w5.to_buf(out,sizeof(out)).put_json(a1);
    assert w5.len() == 13;
    assert memcmp(out,"[\"abc\",\"xyz\"]",13) == 0;

    Queue[JsonValue] a2;
    a2.new().new[String]().set("abc");
    a2.new().new[int]().set(15);
    Writer w6.to_buf(out,sizeof(out)).put_json(a2);
    assert w6.len() == 10;
    assert memcmp(out,"[\"abc\",15]",10) == 0;

    Map[String,String::cmp,JsonValue] m1;
    auto m1e1 = m1.new();
    m1e1.k.set("k1");
    m1e1.v.new[String]().set("v1");
    auto m1e2 = m1.new();
    m1e2.k.set("k2");
    Writer w7.to_buf(out,sizeof(out)).put_json(m1.sweep());
    assert w7.len() == 21;
    assert memcmp(out,"{\"k1\":\"v1\",\"k2\":null}",21) == 0;

    String j1 = m1.to_string();
    assert j1.eqs("{\"k1\":\"v1\",\"k2\":null}");
}

/////////////////////////////////

include "writer";
include "std/string";
include "coll/objectcontainer";
include "coll/map"; // for Pair

public trait JsonValue {
    void to_json(const Self self, Writer* writer != null);
}

public [J:JsonValue] inline Self* Writer::put_json(Self* self != null, const J value) {
    value.to_json(self);
    return self;
}

public [J:JsonValue] inline String J::to_string(const J value) {
    String result;
    Writer writer.to_buf(null,0);
    writer.put_json(value); // get length
    writer.to_buf(result.new(writer.len()),writer.len()+1);
    writer.put_json(value); // write json
    writer.putc('\0');
    return result;
}

/////////////////////////////////

public augment JsonValue with JsonValue {
    void to_json(const Self self, Writer* writer != null) {
        if (self.is[void]()) {
            writer.put("null",4);
        } else {
            self.to_json:disp(writer);
        }
    }
}

public augment String with JsonValue {
    void to_json(const Self self, Writer* writer != null) {
        // nullstring serializes as "null"
        if (self.str() == null) {
            writer.put("null",4);
        }
        // write output
        writer.putc('"');
        for (const char* p = self.str(); *p; ++p) {
            switch (*p) {
                case '"':   writer.putc('\\'); writer.putc('"'); break;
                case '\\':  writer.putc('\\'); writer.putc('\\'); break;
                case '\t':  writer.putc('\\'); writer.putc('t'); break;
                case '\r':  writer.putc('\\'); writer.putc('r'); break;
                case '\n':  writer.putc('\\'); writer.putc('n'); break;
                case '\b':  writer.putc('\\'); writer.putc('b'); break;
                case '\f':  writer.putc('\\'); writer.putc('f'); break;
                default:    writer.putc(*p); break;
            }
        }
        writer.putc('"');
    }
}

public augment bool with JsonValue {
    void to_json(const Self self, Writer* writer != null) {
        if (self) {
            writer.put("true",4);
        } else {
            writer.put("false",5);
        }
    }
}

public augment byte with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%d",(const int)self);
    }
}

public augment ubyte with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%u",(const uint)self);
    }
}

public augment short with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%d",(const int)self);
    }
}

public augment ushort with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%u",(const uint)self);
    }
}

public augment int with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%d",self);
    }
}

public augment uint with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%u",self);
    }
}

public augment long with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%ld",self);
    }
}

public augment ulong with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%lu",self);
    }
}

public augment llong with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%lld",self);
    }
}

public augment ullong with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%llu",self);
    }
}

public augment float with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%e",self);
    }
}

public augment double with JsonValue {
    inline void to_json(const Self self, Writer* writer != null) {
        writer.writefmt("%le",self);
    }
}

// augment collections of JsonValue implementers
public [J:JsonValue,P:ObjectProvider[J,Writer]] augment P with JsonValue {
    void to_json(const Self self, Writer* writer != null) {
        writer.putc('[');
        Writer sub_writer.to_buf(writer.wptr(),writer.cap()).inject_obj[J,Writer,P](self, inline op(writer,val) {
            if (writer.len() > 0) writer.putc(',');
            writer.put_json(val);
        });
        writer.commit(sub_writer.len());
        writer.putc(']');
    }
}

// augment maps of JsonValue implementers
// this should work, but is too complex ATM:
//public [K:JsonValue,V:JsonValue,P:ObjectProvider[Pair[K,V],Writer]] augment P with JsonValue {
public [c:cmp[String],V:JsonValue] augment Map[String,c,V] with JsonValue {
    void to_json(const Self self, Writer* writer != null) {
        writer.putc('{');
        Writer sub_writer.to_buf(writer.wptr(),writer.cap()).inject_obj(self, inline op(writer,entry) {
            if (writer.len() > 0) writer.putc(',');
            writer.put_json(entry.k);
            writer.putc(':');
            writer.put_json(entry.v);
        });
        writer.commit(sub_writer.len());
        writer.putc('}');
    }
}

/////////////////////////////////////////

private [T] void Writer::writefmt(Self* self != null, const char* fmt, const T value) {
    uint len = snprintf(self.wptr(),self.cap(),fmt,value);
    // if value would fit except for the trailing '\0', write to a temporary buffer
    if (len == self.cap()) {
        static char buf[100];
        snprintf(buf,sizeof(buf),fmt,value);
        memcpy(self.wptr(),buf,len);
    }
    // in any case advance size
    self.commit(len);
}

native include "string.h","stdio.h" {
    type size_t like uint;
    void *memcpy(void *dest, const void *src, size_t n);
    int snprintf(char *str, size_t size, const char *format, ...);
}

