/* Copyright (c) 2020 Raffin Kuno

  This file is in the pulic domain.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

test "SSL cheatsheet" {
   SSLCtx ctx;
   assert Address addr.tcp("localhost","25") == 0;
   SSLSocket sock = ctx.new_client_socket(addr.connect());
}

////////////////////
// library initialization using single instance of a private class

#define[PUNC_OPENSSL];

@link "ssl"
@link "crypto"
private type SSLInit like int {
on_init:
   SSL_library_init();
   SSL_load_error_strings();
}
private SSLInit sslinit;

////////////////////
// SSL Context

public type SSLCtx like SSL_CTX* = null {
   inline on_init:
      *self:base = SSL_CTX_new(TLS_method());
      SSL_CTX_set_mode(self:base,SSL_MODE_ENABLE_PARTIAL_WRITE);
   inline on_fini:
      SSL_CTX_free(*self:base);
}

public augment SSLCtx {
   bool load_certificate(const SSLCtx self, const char* cert_file, const char* key_file) {
      SSL_CTX_load_verify_locations(self:base, cert_file,null) or return false;
      SSL_CTX_use_certificate_file(self:base, cert_file, SSL_FILETYPE_PEM) or return false;
      SSL_CTX_use_PrivateKey_file(self:base, key_file, SSL_FILETYPE_PEM) or return false;
      SSL_CTX_check_private_key(self:base) or return false;
      return true;
   }

   // TODO do not assert if something went wrong
   SSLSocket new_client_socket(const SSLCtx self, const Socket tcpsock) {
      SSLSocket sslsock;
      sslsock.ssl = SSL_new(self:base) or assert;
      SSL_set_fd(sslsock.ssl,tcpsock.fd()) or assert;
      SSL_connect(sslsock.ssl) or assert;
      return sslsock;
   }

   SSLSocket new_server_socket(const SSLCtx self, const Socket tcpsock) {
      SSLSocket sslsock;
      sslsock.ssl = SSL_new(self:base) or assert;
      SSL_set_fd(sslsock.ssl,tcpsock.fd()) or assert;
      SSL_accept(sslsock.ssl) or assert;
      return sslsock;
   }
}

////////////////////
// SSL

public type SSLSocket {
   SSL* ssl = null;
   int err = SSL_ERROR_NONE;
inline on_move(self,other):
   self.ssl = other.ssl;
   other.ssl = null;
inline on_fini(self):
   SSL_free(self.ssl);
}

public augment SSLSocket {
   inline int last_error(const SSLSocket self) { return self.err; }   
   inline int pending(const SSLSocket self) { return SSL_pending(self.ssl); }
   inline int get_shutdown(const SSLSocket self) { return SSL_get_shutdown(self.ssl); }
   inline int shutdown(SSLSocket* self) {
      int res = SSL_shutdown(self.ssl);
      self.err = SSL_get_error(self.ssl,res);
      return res;
   }
}

public [T] augment SSLSocket {
   inline int read(SSLSocket* self, T* objects != null, uint object_count) {
      int res = SSL_read(self.ssl,objects,sizeof(T)*object_count);
      self.err = SSL_get_error(self.ssl,res);
      return res;
   }

   inline int write(SSLSocket* self, const T* objects != null, uint object_count) {
      int res = SSL_write(self.ssl,objects,sizeof(T)*object_count);
      self.err = SSL_get_error(self.ssl,res);
      return res;
   }
}

public native include "openssl/ssl.h" {
   const int SSL_ERROR_NONE;
   const int SSL_ERROR_ZERO_RETURN;
   const int SSL_ERROR_WANT_READ;
   const int SSL_ERROR_WANT_WRITE;
   const int SSL_ERROR_WANT_CONNECT;
   const int SSL_ERROR_WANT_ACCEPT;
   const int SSL_ERROR_WANT_X509_LOOKUP;
   const int SSL_ERROR_SYSCALL;
   const int SSL_ERROR_SSL;
   
   const int SSL_SENT_SHUTDOWN;
   const int SSL_RECEIVED_SHUTDOWN;
}

////////////////////

include "std/socket";

private native include "openssl/ssl.h" {
   int SSL_library_init();
   void SSL_load_error_strings();
   
   type SSL_METHOD;
   SSL_METHOD* TLS_method();
   
   type SSL_CTX;
   SSL_CTX* SSL_CTX_new(const SSL_METHOD *method);
   void SSL_CTX_free(SSL_CTX *ctx);

   const long SSL_MODE_ENABLE_PARTIAL_WRITE;
   long SSL_CTX_set_mode(SSL_CTX *ctx, long mode);

   const int SSL_FILETYPE_PEM;
   int SSL_CTX_load_verify_locations(SSL_CTX *ctx, const char *CAfile, const char *CApath);
   int SSL_CTX_set_default_verify_paths(SSL_CTX *ctx);
   int SSL_CTX_use_certificate_file(SSL_CTX *ctx, const char *file, int typ);
   int SSL_CTX_use_PrivateKey_file(SSL_CTX *ctx, const char *file, int typ);
   int SSL_CTX_check_private_key(const SSL_CTX *ctx);

   type SSL;
   SSL* SSL_new(SSL_CTX *ctx);
   void SSL_free(SSL *ssl);
   

   int SSL_set_fd(SSL *ssl, int fd);
   //void SSL_set_connect_state(SSL *ssl);
   //void SSL_set_accept_state(SSL *ssl);
   int SSL_connect(SSL *ssl);
   int SSL_accept(SSL *ssl);
   int SSL_read(SSL *ssl, void *buf, int num);
   int SSL_write(SSL *ssl, const void *buf, int num);
   int SSL_pending(const SSL *ssl);
   
   int SSL_get_error(const SSL *ssl, int ret);
   
   int SSL_shutdown(SSL *ssl);
   int SSL_get_shutdown(const SSL *ssl);
}


