#! /bin/bash

DIR=`pwd`

export PUNC_EXEC_PATH=$DIR/bootstrap_phase2/punc
#export PUNC_EXEC_PATH=$DIR/punc
export PUNC_INCLUDE_PATH=$DIR/lib

cd test
rm *.c *.h test

#valgrind --num-callers=40 --leak-check=full $PUNC_EXEC_PATH --inlines=2 --verbose=2 --test=all test.ac || exit 1
$PUNC_EXEC_PATH --inlines=explicit --verbose=1 --test=all --out=test.c test.ac || exit 1
#$PUNC_EXEC_PATH --inlines=explicit --verbose=1 --test=files --out=test.c $PUNC_INCLUDE_PATH/opt/json $PUNC_INCLUDE_PATH/opt/jsonparser || exit 1
#$PUNC_EXEC_PATH --inlines=explicit --verbose=1 --test=files --out=test.c $PUNC_INCLUDE_PATH/net/http/parser || exit 1
#$PUNC_EXEC_PATH --inlines=explicit --verbose=1 --test=files --out=test.c $PUNC_INCLUDE_PATH/net/smtpclient $PUNC_INCLUDE_PATH/ext/openssl || exit 1
#valgrind $PUNC_EXEC_PATH --inlines=2 --verbose=1 --test=files --out=test.c test.ac || exit 1
#valgrind $PUNC_EXEC_PATH --inlines=none --verbose=1 --test=files $PUNC_INCLUDE_PATH/net/transport/tcplistener || exit 1
#$PUNC_EXEC_PATH --inlines=none --verbose=1 --test=files $PUNC_INCLUDE_PATH/net/smtpclient $PUNC_INCLUDE_PATH/ext/openssl || exit 1
#valgrind --num-callers=40 $PUNC_EXEC_PATH --inlines=none --verbose=1 --test=files $PUNC_INCLUDE_PATH/net/httpserver || exit 1
#$PUNC_EXEC_PATH --inlines=none --verbose=1 --test=files --out=test.c $PUNC_INCLUDE_PATH/net/http/parser || exit 1
gcc -g -Wall -Wimplicit-function-declaration *.c -o test -lpthread -lssl -lcrypto || exit 1
valgrind --tool=memcheck --leak-check=full ./test
#valgrind --tool=helgrind ./test
echo

cd ..

